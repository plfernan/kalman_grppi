# Placeholders for machine specific flags
AMPLIFIER_PATH = /afs/cern.ch/sw/IntelSoftware/linux/x86_64/xe2017/vtune_amplifier_xe_2017
# ADDITIONAL_CXX_FLAGS = -fmax-errors=5 -I/opt/intel/tbb/include -I/afs/cern.ch/sw/IntelSoftware/linux/x86_64/xe2017/advisor_2017.1.0.477503/include
# ADDITIONAL_LINKFLAGS = -L/opt/intel/tbb/lib/intel64_lin/gcc4.7 
ADDITIONAL_LINKFLAGS = ${AMPLIFIER_PATH}/lib64/libittnotify.a
ADDITIONAL_CXX_FLAGS = -I${AMPLIFIER_PATH}/include/ -lpthread
GRPPI_CXX_FLAGS = -lboost_system -lboost_thread

ifneq (,$(findstring icc,${CXX}))
	TUNE = -xMIC-AVX512 -no-inline-max-size
	ITT_ON = "yes"
else
	CXX = g++ 
	TUNE = -march=native
endif

TARGET = kalman_grppi
CXXFLAGS = -g -std=c++14 ${TUNE} ${ADDITIONAL_CXX_FLAGS} ${GRPPI_CXX_FLAGS}
LINKFLAGS = -ldl -ltbb -lrt -lhwloc ${ADDITIONAL_LINKFLAGS}
OPTIMIZATION = -O2 -DNDEBUG -DTBB_ON -DGRPPI_HWLOC
DEBUG_VARS = -O0 -DDEBUG -DTBB_ON
SOURCES = main.cpp oldfit/Types.cpp oldfit/Update.cpp oldfit/Predict.cpp oldfit/Similarity.cpp oldfit/MatrixTypes.cpp oldfit/Bismooth.cpp utilities/Tools.cpp
OBJECTS = $(SOURCES:.cpp=.o)

$(TARGET): CXXFLAGS += ${OPTIMIZATION}
$(TARGET): executable

debug: CXXFLAGS += ${DEBUG_VARS}
debug: executable

executable: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(TARGET) $(LINKFLAGS) $(CXXFLAGS)

.cpp.o:
	$(CXX) -c $(CXXFLAGS) -o $@ $<

clean:
	rm -f $(OBJECTS) $(TARGET)
