#!/bin/sh
echo "Running iterations"

path="/data/plfernan/uc3m-paper-results/"
file="grppi3_full_"
ext="thr.txt"
threads=(256 128 64 32 16 8 4)
# threads=(256)
# threads=(4)

for t in "${threads[@]}"
do
	for i in {1..3}
	do
		echo "iteration: $i, threads: ${t}"
		bin="./kalman_grppi_${t} -w50k -n 100k"
		eval "$bin >> $path$file"${t}"$ext"
	done
done

# iter for 2 threads
for i in {1..3}
do
	echo "iteration: $i, thread: 2"
	bin="./kalman_grppi_2 -w50k -n 200k"
	eval "$bin >> $path$file"2"$ext"
done

# iter for 1 thread
for i in {1..3}
do
	echo "iteration: $i, thread: 1"
	bin="./kalman_grppi_1 -w50k -n 400k"
	eval "$bin >> $path$file"1"$ext"
done