#!/bin/sh
echo "Running iterations"
declare -a procs=(0 0-1 0-3 0-7 0-15 0-31 0-63 0-127 0-255)

PERF="perf stat"
RUN="numactl -C "
RUN1=" ./cross_kalman -n "
EXPERIMENTS=10000

for i in "${procs[@]}"
do
    for run in {1..5}
    do
        eval "$PERF$RUN$i$RUN1$EXPERIMENTS" >> perf_results.txt
	echo "repeating experiment $run"
    done
    echo "next iteration $i"
done
