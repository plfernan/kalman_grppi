LHCb Kalman Filter with GrPPI
================================

Welcome to the LHCb Kalman Filter with GrPPI project!

The aim of this project is to produce a *fast* and *efficient* Kalman Filter, while preserving *correctness* of results,
expressing it in terms of parallel patterns in a easy and intuitive way by using the GrPPI interface.

- [GrPPI](https://github.com/arcosuc3m/grppi)

Compiling and running
---------------------

Getting the latest and greatest,

```
# get repo
git clone https://gitlab.cern.ch/plfernan/kalman_grppi.git
cd kalman_grppi
# get GrPPI
git clone https://github.com/arcosuc3m/grppi
# get the events
wget https://lbevent.cern.ch/online/dcampora/kalman_filter/events.tar.gz
tar zxvf events.tar.gz
# Get vectorclass lib
mkdir vectorclass && cd vectorclass
wget http://www.agner.org/optimize/vectorclass.zip
unzip vectorclass.zip
cd ..
```

Compiling and running,

```
mkdir build && cd build
cmake ..
make
```

There are plenty of options available. Have a look at `-h`! For example (open 75 files, run 100000 iterations):

```
./kalman_grppi -m75 -n100k
```

---

To debug:

```
mkdir debug && cd debug
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

To select different compiler:

- Generate new build tree with C and/or CXX environment variables:

```
mkdir build_icc && cd build_icc
CXX=icc cmake ..
make
```

To get detailed compiling info when using cmake:

```
cmake VERBOSE=1
```

---

If you want to compile it for single precision, or even different vector lengths,
this is quite easy,

```
# Compile with single precision, the highest available vector width on the machine
COMPILE_OPTS="-DSP" make

# Compile with single precision and vector width 4
COMPILE_OPTS="-DSP -DSTATIC_VECTOR_WIDTH=4" make

# Compile with icc
CXX=icc make
```
