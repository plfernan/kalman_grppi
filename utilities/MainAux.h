#pragma once

#include <vector>
#include <array>
#include <cmath>
#include <hwloc.h>
#include "../fit/Scheduler.h"
#include "../fit/Store.h"

/**
 * @brief      Initializes datatypes for experiments.
 */
void initializeSchedulerAndIterators(
  std::vector<std::vector<Instance>>& events,
  std::vector<std::vector<std::array<std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>, 3>>>& schedulers,
  std::vector<std::vector<VectorFit::MemManagerIterator>>& nodeParametersIteratorInit,
  std::vector<std::vector<VectorFit::MemManagerIterator>>& transportForwardIteratorInit,
  std::vector<std::vector<VectorFit::MemManagerReverseIterator>>& nodeParametersReverseIteratorInit,
  std::vector<std::vector<VectorFit::MemManagerReverseIterator>>& transportBackwardIteratorInit,
  VectorFit::MemManager& nodeParametersStore,
  VectorFit::MemManager& transportForwardStore,
  VectorFit::MemManager& transportBackwardStore
) {
  for (unsigned i=0; i<events.size(); ++i) {
    auto& event = events[i]; 

    // We reserve mem for the iterators
    nodeParametersIteratorInit[i].resize(event.size());
    transportForwardIteratorInit[i].resize(event.size());
    nodeParametersReverseIteratorInit[i].resize(event.size());
    transportBackwardIteratorInit[i].resize(event.size());
    
    schedulers[i].resize(event.size());

    for (unsigned j=0; j<event.size(); ++j) {
      auto& instance = event[j];
      const auto& aostracks = instance.tracks;
      auto& ntracks = instance.ntracks;

      // we create the schedulers pre, main post ([0][1][2]) 
      schedulers[i][j][VectorFit::Sch::Num::Pre] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Pre, VECTOR_WIDTH>::generate(ntracks);
      schedulers[i][j][VectorFit::Sch::Num::Main] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Main, VECTOR_WIDTH>::generate(ntracks);
      schedulers[i][j][VectorFit::Sch::Num::Post] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Post, VECTOR_WIDTH>::generate(ntracks);

      auto setBasePointers = [&] (
        decltype (schedulers[0][0][0])& scheduler, 
        const bool& populateForward,
        const bool& populateBackward
      ) {
        for (const auto& s : scheduler) {
          const auto& in = s.in;
          const auto& out = s.out;
          const auto& action = s.action;
          const auto& pool = s.pool;

          // ensure vectors are aligned (they are already aligned)
          nodeParametersStore.getNewVector();
          if (populateForward)  { transportForwardStore.getNewVector(); }
          if (populateBackward) { transportBackwardStore.getNewVector(); }

          for (unsigned i=0; i<VECTOR_WIDTH; ++i) { 
            if (action [i]) {
              const auto& aosNode = aostracks[pool[i].trackIndex][pool[i].nodeIndex];
              auto& track = ntracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];

              node.m_nodeParameters.setBasePointer(nodeParametersStore.getLastVector() + i);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ReferenceVector>().copy(aosNode.m_refVector.m_parameters);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ProjectionMatrix>().copy(aosNode.m_projectionMatrix);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ReferenceResidual>() = aosNode.m_refResidual;
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ErrMeasure>() = aosNode.m_errMeasure;

              if (populateForward) {
                node.get<VectorFit::Op::Forward, VectorFit::Op::TransportMatrix>().setBasePointer(transportForwardStore.getLastVector() + i);
                node.get<VectorFit::Op::Forward, VectorFit::Op::TransportMatrix>().copy(aosNode.m_transportMatrix);
              }

              if (populateBackward) {
                node.get<VectorFit::Op::Backward, VectorFit::Op::TransportMatrix>().setBasePointer(transportBackwardStore.getLastVector() + i);
                node.get<VectorFit::Op::Backward, VectorFit::Op::TransportMatrix>().copy(
                  aostracks[pool[i].trackIndex][pool[i].nodeIndex + 1].m_invertTransportMatrix
                );
              }
            }
          }
        }
      };

      nodeParametersIteratorInit[i][j] = VectorFit::MemManagerIterator(nodeParametersStore, true); // fromCurrent (to get from we can write)
      transportForwardIteratorInit[i][j] = VectorFit::MemManagerIterator(transportForwardStore, true); 

      // true / false indicates whether to populate the transport matrix or not
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Pre], false, true);
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Main], true, true);
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Post], true, false);

      nodeParametersReverseIteratorInit[i][j] = VectorFit::MemManagerReverseIterator(nodeParametersStore);
      transportBackwardIteratorInit[i][j] = VectorFit::MemManagerReverseIterator(transportBackwardStore);
    }
  }

  // We don't need the original tracks datatype anymore
  for (auto& event : events) {
    for (auto& instance : event) {
      instance.tracks.clear();
    }
  }
}

/**
 * @brief      Converts literal quantities to nums
 */
int convertAmount(std::string amount) {
    std::string str2 = "k";
    std::size_t found = amount.find(str2);
    if (found != std::string::npos) {
        amount.replace(found,str2.length(),"000");
        return std::stoi(amount);
    }
    
    str2 = "M";
    found = amount.find(str2);
    if (found != std::string::npos) {
        amount.replace(found,str2.length(),"000000");
        return std::stoi(amount);
    }
    
    return std::stoi(amount);
}

/**
 * @brief      Converts literal quantities to nums
 */
std::vector<std::vector<int>> getThreadsTopology(double num_threads){
  hwloc_topology_t topology;
  
  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);

  int nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  int nbnumanodes = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NODE);

  hwloc_topology_destroy(topology);
  
  int numa_nodes = nbnumanodes/2; // we focus on the nodes with cores for Xeon Phi
  int fixed_cores_per_node = nbcores/numa_nodes;
  if (num_threads < numa_nodes){
    numa_nodes = num_threads;
  }
  int hthr = std::ceil((num_threads/numa_nodes)/fixed_cores_per_node);
  int cores_node = std::ceil(num_threads/numa_nodes/hthr);
  int id;
  
  std::vector<std::vector<int>> numa_threads;
  
  for (int i=0; i < numa_nodes; ++i) {
      std::vector<int> aux;
      for (int j=0; j<hthr; ++j) {    
          id = (j * cores_node * numa_nodes) + (i * fixed_cores_per_node);
          for (int k=0; k<cores_node; ++k){
              aux.push_back(id);
              id++;
          }
      }
      numa_threads.push_back(aux);
  }

  return numa_threads;
}