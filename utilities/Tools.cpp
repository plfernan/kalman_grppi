#include "Tools.h"

// #define M_LOG2E 1.44269504088896340736 //log2(e)
inline long double log2(const long long x) {
    return log(x) * M_LOG2E;
}

void print (uint16_t b) {
  quick_and_dirty* a = (quick_and_dirty*) &b;
  std::cout << a->p << a->o << a->n << a->m;
  #if VECTOR_WIDTH >= 8
  std::cout << a->l << a->k << a->j << a->i;
  #endif
  #if VECTOR_WIDTH == 16
  std::cout << a->h << a->g << a->f << a->e
    << a->d << a->c << a->b << a->a;
  #endif
  std::cout << " ";
}

void print(const TrackVector& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<5; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const TrackMatrix& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<25; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const TrackSymMatrix& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<15; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const ChiSquare& c, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  m_chi2: " << c.m_chi2 << std::endl
    << "  m_nDoF: " << c.m_nDoF << std::endl;
}

void print(const XYZVector& x, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  x, y, z: " << x.fX << ", " << x.fY << ", " << x.fZ << std::endl;
}

void print(const State& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  // std::cout << "  m_flags: " << s.m_flags << std::endl;
  print(s.m_stateVector, "  m_stateVector");
  print(s.m_covariance, "  m_covariance");
  // std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const StateVector& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  print(s.m_parameters, "  m_parameters");
  // std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const Node& n, const std::string& name) {
  std::cout << name << ":" << std::endl;
  print(n.m_state, " smoothed state");
  std::cout << " Type: " << n.m_type << std::endl;
  print(n.m_refVector, " reference vector");
  print(n.m_projectionMatrix, " Projection Matrix");
  // std::cout << " m_refIsSet: " << n.m_refIsSet << std::endl
    // << " m_measurement: " << n.m_measurement << std::endl
  std::cout 
    // << " m_residual: " << n.m_residual << std::endl
    // << " m_errResidual: " << n.m_errResidual << std::endl
    << " Error measure: " << n.m_errMeasure << std::endl;
  
}

void print(const FitNode& f, const std::string& name) {
  std::cout << name << ": " << std::endl;
  // print(f.m_noiseMatrix, " m_noiseMatrix");
  // std::cout << " m_deltaEnergy: " << f.m_deltaEnergy << std::endl
  //   << " m_filterStatus: " << f.m_filterStatus[0] << ", " << f.m_filterStatus[1] << std::endl
  //   << " m_hasInfoUpstream: " << f.m_hasInfoUpstream[0] << ", " << f.m_hasInfoUpstream[1] << std::endl;
  std::cout << " Forward:" << std::endl;
  print(f.m_predictedState[0], " predicted state");
  print(f.m_filteredState[0], " updated state");
  print(f.m_totalChi2[0], " chi2");
  std::cout << " Backward:" << std::endl;
  // print(f.m_predictedState[1], " predicted state");
  print(f.m_filteredState[1], " updated state");
  print(f.m_totalChi2[1], " chi2");
  print(*(dynamic_cast<Node*>(& const_cast<FitNode&>(f))), " Node");
  std::cout << " Reference Residual: " << f.m_refResidual << std::endl;
  // print(f.m_transportMatrix, " Transport Matrix");
  // print(f.m_invertTransportMatrix, " m_invertTransportMatrix");
  // print(f.m_transportVector, " m_transportVector");
  // print(f.m_classicalSmoothedState, " m_classicalSmoothedState");
  // print(f.m_deltaChi2[0], " m_deltaChi2[0]");
  // print(f.m_deltaChi2[1], " m_deltaChi2[1]");
  // print(f.m_smootherGainMatrix, " m_smootherGainMatrix");
  // print(f.m_pocaVector, " m_pocaVector");
  // std::cout << " m_doca: " << f.m_doca << std::endl
  //   << " m_chi2Double: " << f.m_chi2Double << std::endl;
}

bool compare(const double& d1, const double& d2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&d1);
  const long long* i2 = reinterpret_cast<const long long*>(&d2);

  long long epsilon = 0;
  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackVector& m1, const TrackVector& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<5; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackMatrix& m1, const TrackMatrix& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<25; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));
    
    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const TrackSymMatrix& m1, const TrackSymMatrix& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<15; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&c1.m_chi2);
  const long long* i2 = reinterpret_cast<const long long*>(&c2.m_chi2);

  long long epsilon = 0;
  bool equal = true;

  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  if (epsilon > max_epsilon) {
    equal = false;
    if (doPrint) std::cout << "Chi2 mismatch " << "(" << "bit " << floor(log2(epsilon)) << ") "
      << "(" << c1.m_chi2 << ", " << c2.m_chi2 << ")" << std::endl;
  }

  if (c1.m_nDoF != c2.m_nDoF) {
    equal = false;
    if (doPrint) std::cout << "nDoF mismatch " << "(" << c1.m_nDoF << ", " << c2.m_nDoF << ")" << std::endl;
  }

  return equal;
}

bool compare(const State& s1, const State& s2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&s1.m_z);
  const long long* i2 = reinterpret_cast<const long long*>(&s2.m_z);

  bool equal = true;

  if (!compare(s1.m_stateVector, s2.m_stateVector, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "StateVector mismatch" << std::endl;
    equal = false;
  }

  if (!compare(s1.m_covariance, s2.m_covariance, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "Covariance mismatch" << std::endl;
    equal = false;
  }

  return equal;
}

bool compare(
  const FitNode& f1,
  const FitNode& f2,
  const double max_epsilon,
  const bool doPrint,
  const int compareChi2
) {
  // For now, let's compare the matrices
  bool equal = true;

  auto test = [&equal, &doPrint] (const bool& result, const std::string& printout) {
    if (!result) {
      equal = false;
      if (doPrint) {
        std::cout << printout << std::endl;
      }
    }
  };
  
  // Compare states
  test(compare(f1.m_filteredState[0], f2.m_filteredState[0], max_epsilon, doPrint), "Forward updated state mismatch");
  test(compare(f1.m_filteredState[1], f2.m_filteredState[1], max_epsilon, doPrint), "Backward updated state mismatch");
  test(compare(f1.m_state, f2.m_state, max_epsilon, doPrint), "Smoothed State mismatch");

  if (compareChi2 == 0)
    test(compare(f1.m_totalChi2[0], f2.m_totalChi2[0], max_epsilon, doPrint), "Forward chi2 mismatch");
  else if (compareChi2 == 1)
    test(compare(f1.m_totalChi2[1], f2.m_totalChi2[1], max_epsilon, doPrint), "Backward chi2 mismatch");

  test(compare(f1.m_residual, f2.m_residual, max_epsilon, doPrint), "Residual mismatch");
  test(compare(f1.m_errResidual, f2.m_errResidual, max_epsilon, doPrint), "Error Residual mismatch");

  return equal;
}

void setPrevNode(FitNode& node, FitNode& prevnode, int direction) {
  if (direction==Forward) node.m_prevNode = &prevnode;
  else node.m_nextNode = &prevnode;
}

std::vector<Instance> translateFileIntoEvent (
  const std::vector<uint8_t>& fileContents,
  VectorFit::MemManager& memManager,
  const bool checkResults,
  const unsigned minTrackBatchSize
) {
  std::vector<Instance> t;
  uint8_t* p = const_cast<uint8_t*>(fileContents.data());

  while (p < fileContents.data() + fileContents.size()) {
    Instance instance;

    // instance.doForwardFit = (bool) *p; p += sizeof(uint8_t);
    // instance.doBackwardFit = (bool) *p; p += sizeof(uint8_t);
    p += sizeof(uint8_t);
    instance.doBismooth = (bool) *p; p += sizeof(uint8_t);
    int numberOfTracks = *((int*) p); p += sizeof(uint32_t);

    instance.tracks.resize(numberOfTracks);
    if (checkResults) {
      instance.expectedResult.resize(numberOfTracks);
    }

    for (int i=0; i<numberOfTracks; ++i) {
      int memblockSize = *((int*) p); p += sizeof(uint32_t);
      
      instance.tracks[i].resize(memblockSize);
      std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.tracks[i].data()));
      p += memblockSize * sizeof(FitNode);

      if (checkResults) {
        instance.expectedResult[i].resize(memblockSize);
        std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.expectedResult[i].data()));
      }
      p += memblockSize * sizeof(FitNode);
    }
    
    // Revise all this when new datatype is there
    instance.doForwardFit = false;
    instance.doBackwardFit = false;

    if (instance.tracks.size() > 0 && instance.tracks[0].size() > 0) {
      auto doFit = [&] (const int& direction) {
        for (const auto& t : instance.tracks) {
          for (const auto& n : t) {
            if (n.requirePredictedState(direction)) {
              return true;
            }
          }
        }
        return false;
      };

      instance.doForwardFit = doFit(Forward);
      instance.doBackwardFit = instance.doForwardFit || doFit(Backward);

      auto isOutlier = [&] {
        for (const auto& n : instance.tracks[0]) {
          if (!n.requirePredictedState(Forward) || !n.requirePredictedState(Forward)) {
            return true;
          }
        }
        return false;
      };
      instance.isOutlier = (instance.doForwardFit || instance.doBackwardFit) && isOutlier();

      if (!instance.isOutlier &&
        instance.doForwardFit &&
        instance.doBackwardFit &&
        instance.doBismooth &&
        instance.tracks.size() >= minTrackBatchSize
      ) {
        // Generate new tracks
        for (int i=0; i<instance.tracks.size(); ++i) {
          instance.ntracks.push_back(VectorFit::Track(instance.tracks[i], i, memManager));
        }

        // Add info on outlier
        // if (instance.isOutlier) {
        //   for (int i=0; i<instance.ntracks.size(); ++i) {
        //     instance.ntracks[i].m_outlier = instance.outliers[i];
        //   }
        // }

        // For the moment, we don't need the original tracks datatype
        // instance.tracks.clear();

        t.push_back(instance);
      }
    }
  }

  assert(p == fileContents.data() + fileContents.size());
  return t;
}

void printInfo(const std::vector<Instance>& t) {
  std::vector<InstanceInfo> instancesInfo;

  for (const auto& instance : t) {
    InstanceInfo info;
    for (const auto& track : instance.ntracks) {
      info.numberOfNodes += track.m_nodes.size();
      for (const auto& node : track.m_nodes) {
        info.numberOfHitNodes += (node.m_type == HitOnTrack);
        info.numberOfReferenceNodes += (node.m_type == Reference);
      }
    }
    instancesInfo.push_back(info);
  }

  unsigned totalNumberOfNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfNodes; return acc; } ();
  unsigned totalNumberOfRefNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfReferenceNodes; return acc; } ();
  unsigned totalNumberOfHitNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfHitNodes; return acc; } ();

  std::cout << " number of instances: " << t.size() << std::endl;
  std::cout << " total number of nodes: " << totalNumberOfNodes
    << " (" << totalNumberOfHitNodes << " hits, " << totalNumberOfRefNodes << " references)" << std::endl;

  std::cout << " #tracks in each instance: ";
  for (unsigned i=0; i<t.size(); ++i) {
    std::cout << t[i].ntracks.size();
    if (i != t.size() - 1) { std::cout << ", "; }
  }
  std::cout << std::endl;

  std::cout << " #nodes in each instance: ";
  for (unsigned i=0; i<instancesInfo.size(); ++i) {
    std::cout << instancesInfo[i].numberOfNodes;
    if (i != t.size() - 1) { std::cout << ", "; }
  }
  std::cout << std::endl << std::endl;
}

std::string shellexec (const char* cmd) {
  char buffer[128];
  std::string result = "";
  std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
  if (!pipe) throw std::runtime_error("popen() failed!");
  while (!feof(pipe.get())) {
    if (fgets(buffer, 128, pipe.get()) != NULL)
      result += buffer;
  }
  return result;
}
