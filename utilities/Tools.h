#pragma once

#include "../oldfit/Types.h"
#include "../fit/Types.h"
#include <cassert>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

// Print mask
#pragma pack (push)
struct quick_and_dirty {
  bool p : 1;
  bool o : 1;
  bool n : 1;
  bool m : 1;
  bool l : 1;
  bool k : 1;
  bool j : 1;
  bool i : 1;
  bool h : 1;
  bool g : 1;
  bool f : 1;
  bool e : 1;
  bool d : 1;
  bool c : 1;
  bool b : 1;
  bool a : 1;
};
#pragma pack (pop)

/**
 * @brief      Instance of the kalman filter
 */
struct Instance {
  bool doForwardFit;
  bool doBackwardFit;
  bool doBismooth;
  bool isOutlier;

  std::vector<std::vector<FitNode>> tracks;
  std::vector<std::vector<FitNode>> expectedResult;
  std::vector<int> outliers;

  // New track type
  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> ntracks;
};

/**
 * @brief      Some useful information about the events
 */
struct InstanceInfo {
  unsigned numberOfNodes, numberOfReferenceNodes, numberOfHitNodes;
  InstanceInfo () : numberOfNodes(0), numberOfReferenceNodes(0), numberOfHitNodes(0) {}
};

long double log2 (const long long x);

// Print statements for all
void print (uint16_t b);
void print (const TrackVector& m, const std::string& name="TrackVector");
void print (const TrackMatrix& m, const std::string& name="TrackMatrix");
void print (const TrackSymMatrix& m, const std::string& name="TrackSymMatrix");
void print (const XYZVector& x, const std::string& name="XYZVector");
void print (const ChiSquare& c, const std::string& name="ChiSquare");
void print (const State& s, const std::string& name="State");
void print (const StateVector& s, const std::string& name="StateVector");
void print (const Node& n, const std::string& name="Node");
void print (const FitNode& f, const std::string& name="FitNode");

bool compare (const TrackVector& m1, const TrackVector& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const TrackMatrix& m1, const TrackMatrix& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const TrackSymMatrix& m1, const TrackSymMatrix& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const FitNode& f1, const FitNode& f2, const double max_epsilon=1e-03, const bool doPrint=false, const int compareChi2=-1);

void setPrevNode (FitNode& node, FitNode& prevnode, int direction);

std::vector<Instance> translateFileIntoEvent (
  const std::vector<uint8_t>& fileContents,
  VectorFit::MemManager& memManager,
  const bool checkResults,
  const unsigned minTrackBatchSize
);
bool readBinary (const std::string& filename, std::vector<Instance>& t);
void printInfo (const std::vector<Instance>& instances);

std::string shellexec (const char* cmd);
