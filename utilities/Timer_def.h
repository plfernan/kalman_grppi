/**
 *      Timer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

Timer::Timer () : _append(0), timeDiff(0) {}

inline void Timer::start () {
  clock_gettime(CLOCK_REALTIME, &tstart);
}

inline void Timer::stop () {
  clock_gettime(CLOCK_REALTIME, &tend);
  timeDiff += (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
}

inline void Timer::flush () {
  timeDiff = 0;
}

inline double Timer::getElapsedTime () {
  clock_gettime(CLOCK_REALTIME, &tend);
  timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
  return ((double) timeDiff) / 1000000000.0;
}

inline long long unsigned Timer::getElapsedTimeLong () {
  clock_gettime(CLOCK_REALTIME, &tend);
  timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
  return timeDiff;
}

inline double Timer::get () const {
  return ((double) timeDiff) / 1000000000.0;
}

inline long long unsigned Timer::getLong () const {
  return timeDiff;
}

inline double Timer::getStartTime () const {
  return tstart.tv_sec + tstart.tv_nsec / 1000000000.0;
}

inline double Timer::getEndTime () const {
  return tend.tv_sec + tend.tv_nsec / 1000000000.0;
}

inline double Timer::getCurrentTime () {
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return t.tv_sec + t.tv_nsec / 1000000000.0;
}
