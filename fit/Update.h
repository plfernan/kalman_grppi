#pragma once

#include "Types.h"
#include "FitMath.h"
#include "FitMathNonVec.h"
#include "ArrayGen.h"

namespace VectorFit {

// TODO add predictedState + updatedState for the getters
template<class D>
inline void update (
  FitNode& node
) {
  if (node.m_type == HitOnTrack) {
    FitMathNonVec::update (
      node.get<D, Op::StateVector>(),
      node.get<D, Op::Covariance>(),
      node.get<D, Op::Chi2>(),
      node.get<Op::NodeParameters, Op::ReferenceVector>(),
      node.get<Op::NodeParameters, Op::ProjectionMatrix>(),
      node.get<Op::NodeParameters, Op::ReferenceResidual>(),
      node.get<Op::NodeParameters, Op::ErrMeasure>()
    );
  }
}

// TODO add predictedState + updatedState to this
template<unsigned W>
inline void update_vec (
  const std::array<Sch::Item, W>& n,
  const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
  fp_ptr_64_const rv,
  fp_ptr_64_const pm,
  fp_ptr_64_const rr,
  fp_ptr_64_const em,
  fp_ptr_64 us,
  fp_ptr_64 uc,
  fp_ptr_64 chi2
) {
  FitMathCommon<W>::update (
    n,
    t,
    us,
    uc,
    chi2,
    rv,
    pm,
    rr,
    em
  );
}

}
