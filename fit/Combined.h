#pragma once

#include <array>
#include "Predict.h"
#include "Update.h"

namespace VectorFit {

// Initialise
template<class T>
inline void fit (
  FitNode& node,
  const TrackSymMatrixContiguous& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);

  update<T>(node);
}

template<class T, bool U>
inline void fit (
  FitNode& node,
  const FitNode& prevnode
) {
  predict<T, U>(node, prevnode);
  update<T>(node);
}

template<class T>
struct fit_vec {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
    fp_ptr_64_const tm,
    fp_ptr_64_const rv,
    fp_ptr_64_const pm,
    fp_ptr_64_const rr,
    fp_ptr_64_const em,
    fp_ptr_64_const last_us,
    fp_ptr_64_const last_uc,
    fp_ptr_64 us,
    fp_ptr_64 uc,
    fp_ptr_64 chi2
  ) {
    predict_vec<T>::template op<W> (
      n,
      t,
      tm,
      last_us,
      last_uc,
      us,
      uc
    );

    FitMathCommon<W>::update (
      n,
      t,
      us,
      uc,
      chi2,
      rv,
      pm,
      rr,
      em
    );
  }
};

}
