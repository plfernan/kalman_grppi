#pragma once

#define TO_STRING(s) WRAPPED_TO_STRING(s)
#define WRAPPED_TO_STRING(s) #s

// Set compiling values
#ifdef SP

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__) || defined(__AVX512__)
  #define VECTOR_WIDTH 16u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 8u
#elif defined(__SSE__)
  #define VECTOR_WIDTH 4u
#else
  #define VECTOR_WIDTH 1u
#endif

#else

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__) || defined(__AVX512__)
  #define VECTOR_WIDTH 8u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 4u
#elif defined(__SSE__)
  #define VECTOR_WIDTH 2u
#else
  #define VECTOR_WIDTH 1u
#endif

#endif

#if VECTOR_WIDTH > 1
  // Include vectorclass
  #define MAX_VECTOR_SIZE 512
  #include "../vectorclass/vectorclass.h"
#endif

#ifdef SP
#define PRECISION float
#else
#define PRECISION double
#endif

// Some align definitions
#define ALIGNMENT sizeof(PRECISION) * VECTOR_WIDTH
#define _aligned alignas(ALIGNMENT)
#ifdef __INTEL_COMPILER
#define _type_aligned __attribute__((align_value(ALIGNMENT)))
#else
#define _type_aligned __attribute__((aligned(ALIGNMENT)))
#endif

typedef _type_aligned PRECISION * const __restrict__ fp_ptr_64;
typedef _type_aligned const PRECISION * const __restrict__ fp_ptr_64_const;


template <unsigned W> struct Vectype { using type = PRECISION; using booltype = bool; };

#ifdef SP

#if defined(__AVX512F__) || defined(__AVX512__)
template<> struct Vectype<16> { using type = Vec16f; using booltype = Vec16fb; };
#endif
#if defined(__AVX__)
template<> struct Vectype<8> { using type = Vec8f; using booltype = Vec8fb; };
#endif
#if defined(__SSE__)
template<> struct Vectype<4> { using type = Vec4f; using booltype = Vec4fb; };
#endif

#else

#if defined(__AVX512F__) || defined (__AVX512__)
template<> struct Vectype<8> { using type = Vec8d; using booltype = Vec8db; };
#endif
#if defined(__AVX__)
template<> struct Vectype<4> { using type = Vec4d; using booltype = Vec4db; };
#endif
#if defined(__SSE__)
template<> struct Vectype<2> { using type = Vec2d; using booltype = Vec2db; };
#endif

#endif
