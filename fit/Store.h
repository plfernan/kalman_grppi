#pragma once

#include <vector>
#include "AlignedAllocator.h"
#include "VectorConfiguration.h"
#include "assert.h"

namespace VectorFit {

struct MemManager {
  std::vector<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>> backends;
  unsigned width = 21;
  unsigned currentElement = 0;
  unsigned currentBackend = 0;
  unsigned currentVector = 0;
  unsigned elementCapacity = 0;
  unsigned vectorCapacity = 0;

  MemManager () = default;
  MemManager (const MemManager&) = default;
  MemManager (MemManager&&) = default;
  MemManager& operator= (const MemManager&) = default;
  MemManager& operator= (MemManager&&) = default;

  inline MemManager (
    const unsigned& width,
    const unsigned& numberOfNodes
  ) : width(width) {
    elementCapacity = numberOfNodes;
    backends.emplace_back(elementCapacity * width);
    vectorCapacity = elementCapacity / VECTOR_WIDTH;
  }

  void printState (const std::string& position) {
    std::cout << "State at " << position << std::endl
      << " currentBackend " << currentBackend
      << ", currentVector " << currentVector
      << ", currentElement " << currentElement << std::endl
      << " backend capacity " << backends.size()
      << ", element capacity " << elementCapacity
      << ", vector capacity " << vectorCapacity << std::endl;
  }

  inline void reset () {
    currentBackend = 0;
    currentVector = 0;
    currentElement = 0;
  }

  /**
   * @brief      Aligns to point to a new vector.
   */
  inline void align () {
    if (currentElement != 0) {
      currentElement = 0;
      ++currentVector;

      if (currentVector == vectorCapacity) {
        currentVector = 0;
        ++currentBackend;

        if (currentBackend == backends.size()) {
          backends.emplace_back(elementCapacity * width);
        }
      }
    }
  }

  inline PRECISION* getNewVector () {
    align();

    PRECISION* d = getPointer(currentBackend, currentVector);
    
    ++currentVector;
    if (currentVector == vectorCapacity) {
      currentVector = 0;
      ++currentBackend;

      if (currentBackend == backends.size()) {
        backends.emplace_back(elementCapacity * width);
      }
    }

    return d;
  }

  inline PRECISION* getNextElement () {
    PRECISION* d = getPointer(currentBackend, currentVector, currentElement);

    ++currentElement;
    if (currentElement == VECTOR_WIDTH) {
      ++currentVector;
      currentElement = 0;

      if (currentVector == vectorCapacity) {
        currentVector = 0;
        ++currentBackend;

        if (currentBackend == backends.size()) {
          backends.emplace_back(elementCapacity * width);
        }
      }
    }

    return d;
  }

  inline PRECISION* getLastVector () {
    assert(currentVector!=0 || currentBackend!=0);

    if (currentVector == 0) {
      return getPointer(currentBackend - 1, vectorCapacity - 1);
    }
    return getPointer(currentBackend, currentVector - 1);
  }

  inline PRECISION* getFirstVector () {
    return backends[0].data();
  }

  inline PRECISION* getPointer (
    const unsigned& backend,
    const unsigned& vector,
    const unsigned& element
  ) {
    assert(backend < backends.size() and vector < vectorCapacity and element < VECTOR_WIDTH);

    return backends[backend].data() + vector * VECTOR_WIDTH * width + element;
  }

  inline PRECISION* getPointer (
    const unsigned& backend,
    const unsigned& vector
  ) {
    return getPointer(backend, vector, 0);
  }
};

/**
 * @brief      Effectively a sort of MemManager iterator
 */
struct MemManagerIterator {
  MemManager* memmanager;
  unsigned backend = 0;
  unsigned vector = 0;
  unsigned element = 0;
  unsigned vectorCapacity = 0;

  MemManagerIterator () = default;
  MemManagerIterator (const MemManagerIterator&) = default;
  MemManagerIterator (MemManagerIterator&&) = default;
  MemManagerIterator& operator= (const MemManagerIterator&) = default;
  MemManagerIterator& operator= (MemManagerIterator&&) = default;
  
  MemManagerIterator (MemManager& mm)
    : memmanager(&mm) {
    backend = 0;
    vector = 0;
    element = 0;
    vectorCapacity = memmanager->vectorCapacity;
  }

  MemManagerIterator (MemManager& mm, const bool& fromCurrent)
    : memmanager(&mm) {
    if (fromCurrent) {
      backend = memmanager->currentBackend;
      vector = memmanager->currentVector;
      element = memmanager->currentElement;
      vectorCapacity = memmanager->vectorCapacity;
    } else {
      MemManagerIterator(mm);
    }
  }

  void printState () {
    std::cout << "MemManagerIterator state" << std::endl
      << " backend " << backend
      << ", vector " << vector << std::endl;
  }

  /**
   * @brief      Gets the next memmanager vector,
   *             and updates the internal state
   *
   * @return     Pointer to next vector
   */
  PRECISION* nextVector () {
    assert(
      (memmanager->currentVector==0) ?
      (backend != (memmanager->currentBackend-1) or vector != (vectorCapacity)) :
      (backend != (memmanager->currentBackend) or vector != (memmanager->currentVector))
    );

    PRECISION* d = memmanager->getPointer(backend, vector);

    element = 0;
    ++vector;
    if (vector == vectorCapacity) {
      vector = 0;
      ++backend;
    }

    return d;
  }

  PRECISION* nextElement () {
    assert(backend != (memmanager->currentBackend) or vector != (memmanager->currentVector) or element != (memmanager->currentElement));

    PRECISION* d = memmanager->getPointer(backend, vector, element);

    ++element;
    if (element == VECTOR_WIDTH) {
      element = 0;
      ++vector;
      if (vector == vectorCapacity) {
        vector = 0;
        ++backend;
      }
    }

    return d;
  }
};

/**
 * @brief      Effectively a sort of MemManager reverse iterator
 */
struct MemManagerReverseIterator {
  MemManager* memmanager;
  unsigned backend = 0;
  unsigned vector = 0;
  unsigned vectorCapacity = 0;

  MemManagerReverseIterator () = default;
  MemManagerReverseIterator (const MemManagerReverseIterator&) = default;
  MemManagerReverseIterator (MemManagerReverseIterator&&) = default;
  MemManagerReverseIterator& operator= (const MemManagerReverseIterator&) = default;
  MemManagerReverseIterator& operator= (MemManagerReverseIterator&&) = default;

  MemManagerReverseIterator (MemManager& mm)
    : memmanager(&mm) {
    backend = memmanager->currentBackend;
    vector = memmanager->currentVector;
    vectorCapacity = memmanager->vectorCapacity;
  }

  void printState () {
    std::cout << "MemManagerReverseIterator state" << std::endl
      << " backend " << backend
      << ", vector " << vector << std::endl;
  }

  /**
   * @brief      Gets the previous memmanager vector,
   *             and updates the internal state
   *
   * @return     Pointer to previous vector
   */
  PRECISION* previousVector () {
    assert(backend != 0 or vector != 0);

    if (vector == 0) {
      --backend;
      vector = vectorCapacity;
    }

    return memmanager->getPointer(backend, --vector);
  }
};

}
