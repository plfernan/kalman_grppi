#pragma once

#include <vector>
#include <functional>
#include "Store.h"
#include "Cpp14Compat.h"
#include "VectorConfiguration.h"
#include "MatrixTypes.h"

// We need the old types FitNode, renamed to avoid collisions
#include "../oldfit/Types.h"

typedef Node NodeAOS;
typedef FitNode FitNodeAOS;

// Names for templates
namespace VectorFit {

// Operators accepted for templated get method
namespace Op {
  // Data container
  class NodeParameters;
  class Backward;
  class Forward;
  class Smooth;

  // Data type
  class StateVector;
  class Covariance;
  class Chi2;
  class ReferenceVector;
  class ReferenceResidual;
  class Residual;
  class ErrResidual;
  class ErrMeasure;
  class TransportMatrix;
  class ProjectionMatrix;
}

struct NodeParameters {
  PRECISION* m_basePointer = 0x0;
  TrackVector m_referenceVector;
  TrackVector m_projectionMatrix;
  PRECISION* m_referenceResidual;
  PRECISION* m_errorMeasure;

  NodeParameters () = default;
  NodeParameters (const NodeParameters& copy) = default;
  NodeParameters (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_referenceVector = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_projectionMatrix = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_referenceResidual = p; p += VECTOR_WIDTH;
    m_errorMeasure = p;
  }

  void setBasePointer (const NodeParameters& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const NodeParameters& s) {
    m_referenceVector.copy(s.m_referenceVector);
    m_projectionMatrix.copy(s.m_projectionMatrix);
    *m_referenceResidual = *s.m_referenceResidual;
    *m_errorMeasure = *s.m_errorMeasure;
  }
};

struct SmoothState {
  PRECISION* m_basePointer = 0x0;
  TrackVector m_state;
  TrackSymMatrix m_covariance;
  PRECISION* m_residual;
  PRECISION* m_errResidual;

  SmoothState () = default;
  SmoothState (const SmoothState& copy) = default;
  SmoothState (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_state = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_covariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_residual = p; p += VECTOR_WIDTH;
    m_errResidual = p;
  }

  void setBasePointer (const SmoothState& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const SmoothState& s) {
    m_state.copy(s.m_state);
    m_covariance.copy(s.m_covariance);
    *m_residual = *s.m_residual;
    *m_errResidual = *s.m_errResidual;
  }
};

struct State {
  PRECISION* m_basePointer = 0x0;
  // TrackVector m_predictedState;
  // TrackSymMatrix m_predictedCovariance;
  TrackVector m_updatedState;
  TrackSymMatrix m_updatedCovariance;
  PRECISION* m_chi2;

  State () = default;
  State (const State& copy) = default;
  State (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    // m_predictedState = TrackVector(p); p += 5 * VECTOR_WIDTH;
    // m_predictedCovariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_updatedState = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_updatedCovariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_chi2 = p;
  }

  void setBasePointer (const State& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const State& s) {
    // m_predictedState.copy(s.m_predictedState);
    // m_predictedCovariance.copy(s.m_predictedCovariance);
    m_updatedState.copy(s.m_updatedState);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }

  // // TODO Why did we get rid of this for the new state?
  // State operator++ () {     
  //   setBasePointer(m_basePointer + 21 * VECTOR_WIDTH);
  //   return *this;   
  // }   
  
  // State operator-- () {     
  //   setBasePointer(m_basePointer - 21 * VECTOR_WIDTH);
  //   return *this;   
  // }
};

struct Node {
  Type m_type;
  int* m_measurement;

  Node () = default;
  Node (const Node& node) = default;
  Node (const FitNodeAOS& node) {
    m_type = node.m_type;
    m_measurement = node.m_measurement;
  }
};

struct FitNode : public Node {
  _aligned TrackVectorContiguous m_transportVector;
  _aligned TrackSymMatrixContiguous m_noiseMatrix;
  unsigned m_index;

  // This types are in the store
  State m_forwardState;
  State m_backwardState;
  TrackMatrix<25> m_forwardTransportMatrix;
  TrackMatrix<25> m_backwardTransportMatrix;
  SmoothState m_smoothState;
  NodeParameters m_nodeParameters;

  FitNode () = default;
  FitNode (const FitNode& node) = default;
  FitNode (const FitNodeAOS& node)
  : Node(node),
    m_noiseMatrix(node.m_noiseMatrix), 
    m_transportVector(node.m_transportVector)
    {}

  FitNode (const FitNodeAOS& node, const unsigned& index)
  : Node(node),
    m_noiseMatrix(node.m_noiseMatrix), 
    m_transportVector(node.m_transportVector),
    m_index(index) 
    {}

  operator FitNodeAOS () const {
    FitNodeAOS n;
    n.m_type = m_type;
    n.m_residual = *(m_smoothState.m_residual);
    n.m_errResidual = *(m_smoothState.m_errResidual);
    n.m_errMeasure = *(m_nodeParameters.m_errorMeasure);
    n.m_projectionMatrix = m_nodeParameters.m_projectionMatrix.operator TrackVectorAOS();
    n.m_refVector = m_nodeParameters.m_referenceVector.operator StateVector();

    n.m_transportVector = m_transportVector.operator TrackVectorAOS();
    n.m_noiseMatrix = m_noiseMatrix.operator TrackSymMatrixAOS();
    n.m_refResidual = *(m_nodeParameters.m_referenceResidual);

    n.m_filteredState[Forward].m_stateVector = m_forwardState.m_updatedState.operator TrackVectorAOS();
    n.m_filteredState[Forward].m_covariance = m_forwardState.m_updatedCovariance.operator TrackSymMatrixAOS();
    n.m_filteredState[Backward].m_stateVector = m_backwardState.m_updatedState.operator TrackVectorAOS();
    n.m_filteredState[Backward].m_covariance = m_backwardState.m_updatedCovariance.operator TrackSymMatrixAOS();

    n.m_state.m_stateVector = m_smoothState.m_state.operator TrackVectorAOS();
    n.m_state.m_covariance = m_smoothState.m_covariance.operator TrackSymMatrixAOS();

    if (m_forwardTransportMatrix.m_basePointer != nullptr) {
      n.m_transportMatrix = m_forwardTransportMatrix.operator TrackMatrixAOS();
    }

    if (m_backwardTransportMatrix.m_basePointer != nullptr) {
      n.m_invertTransportMatrix = m_backwardTransportMatrix.operator TrackMatrixAOS();
    }

    return n;
  }

  // The class U type specifier is conditional to S
  // One could do the same with a partially specialized class,
  // but here we want a function
  template<class R, class S,
    class U = 
      typename std::conditional<std::is_same<S, Op::Covariance>::value,       TrackSymMatrix, 
      typename std::conditional<std::is_same<S, Op::StateVector>::value,      TrackVector,
      typename std::conditional<std::is_same<S, Op::ReferenceVector>::value,  TrackVector,
      typename std::conditional<std::is_same<S, Op::ProjectionMatrix>::value, TrackVector,
      typename std::conditional<std::is_same<S, Op::TransportMatrix>::value,  TrackMatrix<25>, PRECISION>::type>::type>::type>::type>::type
  >
  inline const U& get () const;

  template<class R, class S,
    class U =
      typename std::conditional<std::is_same<S, Op::Covariance>::value,       TrackSymMatrix, 
      typename std::conditional<std::is_same<S, Op::StateVector>::value,      TrackVector,
      typename std::conditional<std::is_same<S, Op::ReferenceVector>::value,  TrackVector,
      typename std::conditional<std::is_same<S, Op::ProjectionMatrix>::value, TrackVector,
      typename std::conditional<std::is_same<S, Op::TransportMatrix>::value,  TrackMatrix<25>, PRECISION>::type>::type>::type>::type>::type
  >
  inline U& get ();
};

#include "NodeGetters.h"

struct Track {
  std::vector<VectorFit::FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> m_nodes;

  // Active measurements from this node
  unsigned m_forwardUpstream;
  unsigned m_backwardUpstream;

  // Chi square of track
  PRECISION m_forwardFitChi2 = 0.0;
  PRECISION m_backwardFitChi2 = 0.0;
  
  // Some other things
  unsigned m_index;
  int m_ndof = 0;
  int m_parent_nTrackParameters;

  // Initial covariances
  TrackSymMatrixContiguous m_initialForwardCovariance;
  TrackSymMatrixContiguous m_initialBackwardCovariance;

  Track () = default;
  Track (const Track& track) = default;
  Track (
    const std::vector<FitNodeAOS>& track,
    const unsigned& trackNumber,
    MemManager& memManager
  ) : m_index(trackNumber) {
    // Generate the nodes
    int i=0;
    for (const auto& node : track) {
      m_nodes.push_back(VectorFit::FitNode(node, i++));
    }

    // Copy the initial forward predicted and backward predicted covariance
    m_initialForwardCovariance  = track.front().m_predictedState[0].m_covariance;
    m_initialBackwardCovariance = track.back().m_predictedState[1].m_covariance;

    // Copy the initial m_parent_trackParameters
    m_parent_nTrackParameters = track.front().m_parent_nTrackParameters;

    // Copy all other state and covariances (to avoid 0x0 accesses - this shouldn't be in Gaudi)
    for (int i=0; i<m_nodes.size(); ++i) {
      const FitNodeAOS& oldnode = track[i];
      FitNode& node = m_nodes[i];

      node.m_forwardState.setBasePointer(VectorFit::State(memManager.getNextElement()));
      node.m_backwardState.setBasePointer(VectorFit::State(memManager.getNextElement()));

      node.get<Op::Forward, Op::StateVector>().copy(oldnode.m_filteredState[0].m_stateVector);
      node.get<Op::Forward, Op::Covariance>().copy(oldnode.m_filteredState[0].m_covariance);
      node.get<Op::Backward, Op::StateVector>().copy(oldnode.m_filteredState[1].m_stateVector);
      node.get<Op::Backward, Op::Covariance>().copy(oldnode.m_filteredState[1].m_covariance);
    }

    m_forwardUpstream = 0;
    m_backwardUpstream = track.size() - 1;

    bool foundForward = false;
    bool foundBackward = false;
    for (int i=0; i<track.size(); ++i) {
      const int reverse_i = track.size() - i - 1;

      if (!foundForward && track[i].m_type == HitOnTrack) {
        foundForward = true;
        m_forwardUpstream = i;
      }

      if (!foundBackward && track[reverse_i].m_type == HitOnTrack) {
        foundBackward = true;
        m_backwardUpstream = i;
      }
    }
  }
};

}
