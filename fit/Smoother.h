#pragma once

#include "Types.h"
#include "FitMath.h"
#include "FitMathNonVec.h"

namespace VectorFit {

template<bool F, bool B>
inline void smoother (
  FitNode& node
);

template<>
inline void smoother<false, true> (
  FitNode& node
) {
  node.get<Op::Smooth, Op::StateVector>().setBasePointer(node.get<Op::Backward, Op::StateVector>());
  node.get<Op::Smooth, Op::Covariance>().setBasePointer(node.get<Op::Backward, Op::Covariance>());
}

template<>
inline void smoother<true, false> (
  FitNode& node
) {
  node.get<Op::Smooth, Op::StateVector>().setBasePointer(node.get<Op::Forward, Op::StateVector>());
  node.get<Op::Smooth, Op::Covariance>().setBasePointer(node.get<Op::Forward, Op::Covariance>());
}

template<>
inline void smoother<true, true> (
  FitNode& node
) {
  FitMathNonVec::average (
    node.get<Op::Forward, Op::StateVector>(),
    node.get<Op::Forward, Op::Covariance>(),
    node.get<Op::Backward, Op::StateVector>(),
    node.get<Op::Backward, Op::Covariance>(),
    node.get<Op::Smooth, Op::StateVector>(),
    node.get<Op::Smooth, Op::Covariance>()
  );
}

inline void updateResiduals (
  FitNode& node
) {
  PRECISION value {0.0}, error {0.0};
  if (node.m_measurement != nullptr) {
    const TrackVector& H = node.get<Op::NodeParameters, Op::ProjectionMatrix>();
    PRECISION HCH;
    FitMathNonVec::similarity_5_1 (
      node.get<Op::Smooth, Op::Covariance>(),
      H,
      &HCH);
    
    // H * (refx - T)
    auto mulSubMatrix = [] (const TrackVector& a, const TrackVector& b, const TrackVector& c) {
      return (a[0] * (b[0] - c[0]) +
              a[1] * (b[1] - c[1]) +
              a[2] * (b[2] - c[2]) +
              a[3] * (b[3] - c[3]) +
              a[4] * (b[4] - c[4]));
    };

    const TrackVector& refX = node.get<Op::NodeParameters, Op::ReferenceVector>();
    const PRECISION V = node.get<Op::NodeParameters, Op::ErrMeasure>() * node.get<Op::NodeParameters, Op::ErrMeasure>();
    const PRECISION sign = (node.m_type == HitOnTrack) ? -1 : 1;
    value = node.get<Op::NodeParameters, Op::ReferenceResidual>() + mulSubMatrix(H, refX, node.get<Op::Smooth, Op::StateVector>());
    error = V + sign * HCH;
  }
  *node.m_smoothState.m_residual = value;
  *node.m_smoothState.m_errResidual = error;
}

template<long unsigned W>
inline uint16_t smoother_vec (
  fp_ptr_64_const s1,
  fp_ptr_64_const s2,
  fp_ptr_64_const c1,
  fp_ptr_64_const c2,
  fp_ptr_64 ss,
  fp_ptr_64 sc
) {
  return FitMathCommon<W>::average (
    s1,
    c1,
    s2,
    c2,
    ss,
    sc
  );
}

template<long unsigned W>
inline void updateResiduals_vec (
  const std::array<Sch::Item, W>& n,
  const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
  fp_ptr_64_const rv,
  fp_ptr_64_const pm,
  fp_ptr_64_const rr,
  fp_ptr_64_const em,
  fp_ptr_64_const ss,
  fp_ptr_64_const sc,
  fp_ptr_64 res,
  fp_ptr_64 errRes
) {
  FitMathCommon<W>::updateResiduals (
    n,
    t,
    rv,
    pm,
    rr,
    em,
    ss,
    sc,
    res,
    errRes
  );
}

}
