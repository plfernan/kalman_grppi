#pragma once

#include <bitset>
#include <ostream>
#include "Types.h"

namespace VectorFit {

namespace Sch {

namespace Num {
  enum Opt {
    Pre = 0,
    Main = 1,
    Post = 2
  };
}

struct Item {
  unsigned trackIndex;
  unsigned nodeIndex;
  unsigned lastnodeIndex;

  Item () = default;
  Item (const Item& copy) = default;
  Item (
    const unsigned& trackIndex,
    const unsigned& nodeIndex,
    const unsigned& lastnodeIndex
  ) : trackIndex(trackIndex),
    nodeIndex(nodeIndex),
    lastnodeIndex(lastnodeIndex) {}
};

template<size_t N>
std::bitset<N> reverse (const std::bitset<N>& set) {
  std::bitset<N> result;
  for (size_t i = 0; i < N; i++) {
    result[i] = set[N-i-1];
  }
  return result;
}

template<size_t N>
struct Blueprint {
  std::bitset<N> in;
  std::bitset<N> out;
  std::bitset<N> action;
  std::array<Item, N> pool;

  Blueprint () = default;
  Blueprint (const Blueprint& copy) = default;
  Blueprint (
    const std::bitset<N>& in,
    const std::bitset<N>& out,
    const std::bitset<N>& action,
    const std::array<Item, N>& pool
  ) : in(in), out(out), action(action), pool(pool) {}

  friend inline std::ostream& operator<< (
    std::ostream& cout,
    const std::vector<Blueprint<N>>& plan
  ) {
    for (size_t i=0; i<plan.size(); ++i) {
      auto& item = plan[i];
      cout << "#" << i << ": "
        << reverse<N>(item.in) << " "
        << reverse<N>(item.out) << " "
        << reverse<N>(item.action) << " ";


      auto& pool = item.pool;
      std::cout << "{ ";
      for (unsigned j=0; j<pool.size(); ++j) {
        std::cout << pool[j].trackIndex << "-" << pool[j].nodeIndex << " ";
      }
      std::cout << "}" << std::endl;
    }
    return cout;
  }
};

template<size_t N>
struct StaticSchedulerCommon {
  // Generic generate method
  template<class F0, class F1, class F2>
  static std::vector<Blueprint<N>>
  generate (
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>& tracks,
    const F0& getSize,
    const F1& getElement,
    const F2& getLastElement
  ) {
    if (N > tracks.size()) {
      std::cout << "Scheduler: N <= tracks.size(), things probably won't work" << std::endl;
    }

    // Order tracks
    auto& no_const_tracks = const_cast<std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>&>(tracks);
    std::vector<std::reference_wrapper<VectorFit::Track>> ordered_ntracks (no_const_tracks.begin(), no_const_tracks.end());
    std::sort(ordered_ntracks.begin(), ordered_ntracks.end(), [&getSize] (const VectorFit::Track& t0, const VectorFit::Track& t1) {
      const size_t size0 = getSize(t0);
      const size_t size1 = getSize(t1);
      return size0 > size1;
    });

    // Initialise
    std::vector<Blueprint<N>> plan;
    plan.reserve(1000);
    std::bitset<N> in; // Note: Default constructor, it's initialized with zeroes
    std::bitset<N> out;
    std::bitset<N> action = std::bitset<N>().set();
    std::array<Item, N> pool;
    std::vector<unsigned> slots (N);
    std::iota(slots.begin(), slots.end(), 0);
    auto trackIterator = ordered_ntracks.begin();

    // Iterate over all tracks, add what we need
    while (trackIterator != ordered_ntracks.end()) {
      const VectorFit::Track& track = *trackIterator;

      if (getSize(track) > 0) {
        // Add the element to a free slot
        const unsigned slot = slots.back();
        slots.pop_back();
        in[slot] = true;
        pool[slot] = Item (
          track.m_index,
          getElement(track),
          getLastElement(track)
        );
      }

      while (slots.empty()) {
        // Set out mask for tracks where
        // this is the last node to process
        out = false;
        for (size_t i=0; i<N; ++i) {
          Item& s = pool[i];
          if (s.nodeIndex + 1 == s.lastnodeIndex) {
            out[i] = 1;
            slots.push_back(i);
          }
        }

        // Add to plan
        plan.emplace_back(in, out, action, pool);

        // Initialise in, and prepare nodes for next iteration
        in = false;
        std::for_each (pool.begin(), pool.end(), [] (Item& s) {
          if (s.nodeIndex + 1 != s.lastnodeIndex) {
            ++s.nodeIndex;
          }
        });
      }
      
      ++trackIterator;
    }

    // Iterations with no full vectors
    if (slots.size() < N) {
      // make processingIndices the indices that didn't finish yet
      std::vector<unsigned> processingIndices (N);
      std::iota(processingIndices.begin(), processingIndices.end(), 0);
      for (auto it=slots.rbegin(); it!=slots.rend(); ++it) {
        processingIndices.erase(processingIndices.begin() + *it);
      }

      auto processingIndicesEnd = processingIndices.end();
      while (processingIndicesEnd != processingIndices.begin()) {
        // Set out and action mask
        action = false;
        out = false;
        std::for_each (processingIndices.begin(), processingIndicesEnd, [&] (const unsigned& i) {
          Item& s = pool[i];
          action[i] = 1;
          if (s.nodeIndex + 1 == s.lastnodeIndex) {
            out[i] = true;
          }
        });

        // Adding to the plan
        plan.emplace_back(in, out, action, pool);

        // Initialise in, and prepare nodes for next iteration
        in = false;
        processingIndicesEnd = std::remove_if(processingIndices.begin(), processingIndicesEnd, [&] (const unsigned& i) {
          Item& s = pool[i];
          if (s.nodeIndex + 1 == s.lastnodeIndex) return true;
          ++s.nodeIndex;
          return false;
        });
      }
    }

    return plan;
  }
};

// Specializable generate method
class Pre;
class Main;
class Post;

template<class T, size_t N>
struct StaticScheduler {
  StaticScheduler () = default;
  StaticScheduler (const StaticScheduler& copy) = default;

  static std::vector<Blueprint<N>>
  generate (
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>& tracks
  );
};

template<size_t N>
struct StaticScheduler<Pre, N> {
  static std::vector<Blueprint<N>>
  generate (
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const VectorFit::Track& t) { return (t.m_forwardUpstream + 1); },
      [] (const VectorFit::Track& t) { return 0; },
      [] (const VectorFit::Track& t) { return t.m_forwardUpstream + 1; }
    );
  }
};

template<size_t N>
struct StaticScheduler<Main, N> {
  static std::vector<Blueprint<N>>
  generate (
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const VectorFit::Track& t) { return t.m_nodes.size() - t.m_forwardUpstream - t.m_backwardUpstream; },
      [] (const VectorFit::Track& t) { return t.m_forwardUpstream + 1; },
      [] (const VectorFit::Track& t) { return t.m_nodes.size() - t.m_backwardUpstream - 1; }
    );
  }
};

template<size_t N>
struct StaticScheduler<Post, N> {
  static std::vector<Blueprint<N>>
  generate (
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, ALIGNMENT>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const VectorFit::Track& t) { return (t.m_backwardUpstream + 1); },
      [] (const VectorFit::Track& t) { return t.m_nodes.size() - t.m_backwardUpstream - 1; },
      [] (const VectorFit::Track& t) { return t.m_nodes.size(); }
    );
  }
};

}

struct SwapStore {
  State store;
  TrackVector& state;
  TrackSymMatrix& covariance;

  SwapStore (const State& store, TrackVector& state, TrackSymMatrix& covariance) :
    store(store), state(state), covariance(covariance) {}
};

}
