#pragma once

#include "Types.h"
#include "Scheduler.h"
#include "FitMath.h"
#include "FitMathNonVec.h"
#include "ArrayGen.h"

namespace VectorFit {

template<class T>
inline void transportCovariance (
  const TrackMatrix<25>& tm,
  const T& uc,
  TrackSymMatrix& pc
) {
  // TODO
  // this changed because the operator is not implemented, so
  // tm(0,4) is tm[4], following the rule i*5+j
  bool isLine = tm[4]==0;
  if (!isLine) {
    FitMathNonVec::similarity_5_5(tm, uc, pc);
  } else {
    pc.copy(uc);

    if (tm[2] != 0 or tm[8] != 0) {
      pc(0,0) += 2 * uc(2,0) * tm[2] + uc(2,2) * tm[2] * tm[2];
      pc(2,0) += uc(2,2) * tm[2];
      pc(1,1) += 2 * uc(3,1) * tm[8] + uc(3,3) * tm[8] * tm[8];
      pc(3,1) += uc(3,3) * tm[8];
      pc(1,0) += uc(2,1) * tm[2] + uc(3,0) * tm[8] + uc(3,2) * tm[2] * tm[8];
      pc(2,1) += uc(3,2) * tm[8];
      pc(3,0) += uc(3,2) * tm[2];
    }
  }
}

// Initialise
template<class T>
inline void initialise (
  FitNode& node,
  const TrackSymMatrixContiguous& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);
}

template<class T, bool U>
inline void predict (
  FitNode& node,
  const FitNode& prevnode
);

template<>
inline void predict<Op::Forward, false> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.get<Op::Forward, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<Op::Forward, Op::Covariance>().copy(prevnode.get<Op::Forward, Op::Covariance>());
}

template<>
inline void predict<Op::Forward, true> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.get<Op::Forward, Op::StateVector>().copy(node.m_transportVector);
  for (int i=0; i<5; ++i) {
    for (int j=0; j<5; ++j) {
      node.get<Op::Forward, Op::StateVector>()[i] += 
        node.get<Op::Forward, Op::TransportMatrix>()[5*i + j] *
        prevnode.get<Op::Forward, Op::StateVector>()[j];
    }
  }

  transportCovariance(
    node.get<Op::Forward, Op::TransportMatrix>(),
    prevnode.get<Op::Forward, Op::Covariance>(),
    node.get<Op::Forward, Op::Covariance>());
  node.get<Op::Forward, Op::Covariance>() += node.m_noiseMatrix;
}

template<>
inline void predict<Op::Backward, false> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.get<Op::Backward, Op::StateVector>().copy(node.get<Op::Forward, Op::StateVector>()); // TODO this line implies that forward MUST end before doing backward
  node.get<Op::Backward, Op::Covariance>().copy(prevnode.get<Op::Backward, Op::Covariance>());
}

template<>
inline void predict<Op::Backward, true> (
  FitNode& node,
  const FitNode& prevnode
) {
  TrackVectorContiguous temp_sub;
  for (int i=0; i<5; ++i) {
    temp_sub[i] = prevnode.get<Op::Backward, Op::StateVector>()[i] - prevnode.m_transportVector[i];
  };

  for (int i=0; i<5; ++i) {
    node.get<Op::Backward, Op::StateVector>()[i] = node.get<Op::Backward, Op::TransportMatrix>()[5*i] * temp_sub[0];
    for (int j=1; j<5; ++j) {
      node.get<Op::Backward, Op::StateVector>()[i] += node.get<Op::Backward, Op::TransportMatrix>()[5*i + j] * temp_sub[j];
    }
  }

  TrackSymMatrixContiguous temp_cov;
  for (int i=0; i<15; ++i) {
    temp_cov[i] = prevnode.get<Op::Backward, Op::Covariance>()[i] + prevnode.m_noiseMatrix[i];
  }

  transportCovariance (
    node.get<Op::Backward, Op::TransportMatrix>(),
    temp_cov,
    node.get<Op::Backward, Op::Covariance>()
  );
}

// TODO predicted states need to be added here
template<class T>
struct predict_vec {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  );
};

// Vectorised predicts that assume data is already prepared, aligned and so on

// TODO predicted states need to be added here
template<>
struct predict_vec<Op::Forward> {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    _aligned const std::array<PRECISION, 15*W> nm = ArrayGen::getCurrentNoiseMatrix(n, t);
    _aligned const std::array<PRECISION, 5*W> tv = ArrayGen::getCurrentTransportVector(n, t);

    FitMath<Op::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Op::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

// TODO predicted states need to be added here
template<>
struct predict_vec<Op::Backward> {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& t,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    _aligned const std::array<PRECISION, 5*W> tv = ArrayGen::getPreviousTransportVector(n, t);
    _aligned std::array<PRECISION, 15*W> nm = ArrayGen::getPreviousNoiseMatrix(n, t);

    FitMath<Op::Backward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Op::Backward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

}
