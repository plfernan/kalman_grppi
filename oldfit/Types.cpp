#include "Types.h"
#include "../fit/Types.h"

void operator+= (ChiSquare& c0, const ChiSquare& c1) {
  c0.m_chi2 += c1.m_chi2;
  c0.m_nDoF += c1.m_nDoF;
}
