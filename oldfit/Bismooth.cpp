#include "Bismooth.h"

void bismooth (FitNode& node) {
  if      (!node.m_hasInfoUpstream[Forward])  node.m_state = node.m_filteredState[Backward];
  else if (!node.m_hasInfoUpstream[Backward]) node.m_state = node.m_filteredState[Forward];
  else {
    const bool condition = node.m_predictedState[Backward].m_covariance(0,0) > node.m_predictedState[Forward].m_covariance(0,0);
    State& s1 = condition ? node.m_filteredState[Backward] : node.m_filteredState[Forward];
    State& s2 = condition ? node.m_predictedState[Forward] : node.m_predictedState[Backward];

    node.m_state.m_z = node.m_refVector.m_z;

    const bool success = math_average(
      s1.m_stateVector.fArray,
      s1.m_covariance.fArray,
      s2.m_stateVector.fArray,
      s2.m_covariance.fArray,
      node.m_state.m_stateVector.fArray,
      node.m_state.m_covariance.fArray);
  }

  {
    double value {0.0}, error {0.0};

    if (node.m_measurement != 0) { // this->hasMeasurement()

      // res = computeResidual(m_state, this->type() == LHCb::Node::HitOnTrack);
      {
        const bool biased = node.m_type == HitOnTrack;
        const TrackProjectionMatrix& H  = node.m_projectionMatrix;
        const TrackVector& refX         = node.m_refVector.m_parameters;
        const double V = node.m_errMeasure * node.m_errMeasure;
        const double sign = biased ? -1 : 1;

        double HCH;
        math_similarity_5_1(
          node.m_state.m_covariance.fArray,
          H.fArray,
          &HCH);

        value = node.m_refResidual + (H * (refX - node.m_state.m_stateVector));
        error = V + sign * HCH;
      }
    }
    node.m_residual = value;
    node.m_errResidual = error;
  }
  
  // node.m_filterStatus[Backward] = Smoothed;
}
