#!/bin/bash

PROCESSOR_COUNT=`cat /proc/cpuinfo | grep processor | tail -n1 | awk '{ print ($3 + 1) / 4 }'`

echo Running scalability tests for programs $@

for i in $@
do
  mkdir -p output/${HOSTNAME}/scalability_${i}
done

i=1
while [ $i -le $PROCESSOR_COUNT ]
do
  echo $i
  for program in $1 $2
  do
    for j in `seq 0 3`
    do
      ( `./scripts/numa_snc4.py ${j} ${i}` ./${program} -w 10000 -n 125000 > output/${HOSTNAME}/scalability_${program}/${i}_${j}.out ) &
    done
    wait
  done
  i=$(($i * 2))
done

