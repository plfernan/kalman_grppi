#!/bin/bash

OUTFILE=$2
DIR=$1

echo number of processors, fit mean, smoother mean, throughput >> $OUTFILE
for i in `ls -rt $DIR | grep out`
do
  echo $i
  FIT_MEAN=`cat $DIR/$i | grep "Fit timers mean:" | awk '{ print $4 }'`
  SMOOTHER_MEAN=`cat $DIR/$i | grep "Smoother timers mean:" | awk '{ print $4 }'`
  THROUGHPUT=`cat $DIR/$i | grep combined | awk '{ print $8 }'`
  PROC_NUM=`cat $DIR/$i | grep "tbb default_num_threads" | awk '{ print $6 }'`
  echo $PROC_NUM, $FIT_MEAN, $SMOOTHER_MEAN, $THROUGHPUT >> $OUTFILE
done

