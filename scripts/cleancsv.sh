#!/bin/bash

MAX_PROCESSORS=256
FILENAME=$1
OUTPUT=temp.file

echo `head -n1 $FILENAME` > $OUTPUT
for i in `seq 0 $MAX_PROCESSORS`
do
  if egrep -q "^${i}," $FILENAME
  then
    echo $i
    echo `cat $FILENAME | egrep "^${i}," | sed "s/,/ /g" | awk -v v="$i" '{ i+=$2; j+=$3; k+=$4; l+=v } END { print l",", i",", j",", k}'` >> $OUTPUT
  fi
done

mv $OUTPUT $FILENAME

