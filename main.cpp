#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <map>
#include <array>
#include <deque>
#include <unistd.h>
#include <atomic>
#include <chrono>

#include "oldfit/Types.h"
#include "oldfit/Predict.h"
#include "oldfit/Update.h"
#include "oldfit/Bismooth.h"

#include "utilities/Tools.h"
#include "utilities/Timer.h"
#include "utilities/FileReader.h"
#include "utilities/MainAux.h"

// New types
#include "fit/Types.h"
#include "fit/Predict.h"
#include "fit/Update.h"
#include "fit/Scheduler.h"
#include "fit/Smoother.h"
#include "fit/Combined.h"

// GrPPI
#include "grppi/include/farm.h"
#include <hwloc.h>

#ifdef TBB_ON
#include "tbb/tbb.h"
#endif

#include "ittnotify.h"

#ifdef SP
#pragma message("Generating code for vector width " TO_STRING(VECTOR_WIDTH) ", single precision")
#else
#pragma message("Generating code for vector width " TO_STRING(VECTOR_WIDTH) ", double precision")
#endif

// aliases for long types
using Schedulers = std::vector<std::vector<std::array<std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>, 3>>>;
using Scheduler = std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>;
using NTracks = std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>;

using namespace grppi;

struct MinimalData {
  MinimalData(
    unsigned eno,
    unsigned ino
  ) :
  eventno(eno),
  instanceno(ino) 
  {};
  MinimalData() {};
  unsigned eventno;
  unsigned instanceno;
};

// forward - pre
void forward_pre_iterations(
  const Scheduler& scheduler_pre, 
  VectorFit::MemManager& memvecforward, 
  NTracks& ntracks, 
  VectorFit::MemManagerIterator& nodeParametersIterator 
  ) {
  std::for_each (scheduler_pre.begin(), scheduler_pre.end(), [&] (decltype(scheduler_pre[0])& s) {
    const auto& in = s.in;
    const auto& out = s.out;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Set memory location, do predict (always in a new vector, no need to swap)
    const auto& vector = memvecforward.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];

        // Update pointer
        node.m_forwardState.setBasePointer(vector + i);

        if (in[i]) {
          VectorFit::initialise<VectorFit::Op::Forward>(node, track.m_initialForwardCovariance);
        } else {
          auto& prevnode = track.m_nodes[pool[i].nodeIndex-1];
          VectorFit::predict<VectorFit::Op::Forward, false>(node, prevnode);
        }
      }
    }

    const auto npVector = VectorFit::NodeParameters {nodeParametersIterator.nextVector()};

    // Update
    VectorFit::State current (vector);
    VectorFit::update_vec<VECTOR_WIDTH> (
      pool,
      ntracks,
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );
  });
}

// forward-main
void forward_main_iterations(
  const Scheduler& scheduler_main,
  VectorFit::MemManager& memvecforward,
  NTracks& ntracks,
  VectorFit::MemManagerIterator& nodeParametersIterator,
  VectorFit::MemManagerIterator& transportForwardIterator,
  VectorFit::MemManager& memseq) {
  // Forward fit
  //  Main iterations
  std::vector<VectorFit::SwapStore> swaps;
  memvecforward.getNewVector();
  std::for_each (scheduler_main.begin(), scheduler_main.end(), [&] (decltype(scheduler_main[0])& s) {
    const auto& in  = s.in;
    const auto& out = s.out;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Feed the data we need in our vector
    const auto& lastVector = memvecforward.getLastVector();
    if (in.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (in[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& prevnode = track.m_nodes[pool[i].nodeIndex - 1];

          VectorFit::State memslot (lastVector + i);
          memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
        }
      }
    }

    // update the pointers requested
    const auto& vector = memvecforward.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];
        
        node.m_forwardState.setBasePointer(vector + i);
      }
    }

    const auto npVector = VectorFit::NodeParameters{nodeParametersIterator.nextVector()};

    // predict and update
    VectorFit::State last (lastVector);
    VectorFit::State current (vector);

    VectorFit::fit_vec<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
      pool,
      ntracks,
      transportForwardIterator.nextVector(),
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      last.m_updatedState.m_basePointer,
      last.m_updatedCovariance.m_basePointer,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );

    // Move requested data out
    if (out.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (out[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& node = track.m_nodes[pool[i].nodeIndex];

          VectorFit::State memslot (memseq.getNextElement());
          memslot.m_updatedState.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
          swaps.push_back(
            VectorFit::SwapStore(
              memslot,
              node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>(),
              node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>()
            )
          );
        }
      }
    }
  });

  // Restore updated state
  for (auto& swap : swaps) {
    swap.state.copy(swap.store.m_updatedState);
    swap.covariance.copy(swap.store.m_updatedCovariance);
  }
}

// forward-post
void forward_post_iterations(
  const Scheduler& scheduler_post,
  VectorFit::MemManager& memvecpost,
  NTracks& ntracks,
  VectorFit::MemManagerIterator& nodeParametersIterator,
  VectorFit::MemManagerIterator& transportForwardIterator,
  VectorFit::MemManager& memseq ) {
  // Forward fit
  //  Post iterations
  memvecpost.getNewVector();
  std::for_each (scheduler_post.begin(), scheduler_post.end(), [&] (decltype(scheduler_post[0])& s) {
    const auto& in  = s.in;
    const auto& out = s.out;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Feed the data we need in our vector
    const auto& lastVector = memvecpost.getLastVector();
    if (in.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (in[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& prevnode = track.m_nodes[pool[i].nodeIndex - 1];
        
          VectorFit::State memslot (lastVector + i);
          memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
        }
      }
    }

    // update the pointers requested
    const auto& vector = memvecpost.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];

        node.m_forwardState.setBasePointer(vector + i);
      }
    }

    const auto npVector = VectorFit::NodeParameters{nodeParametersIterator.nextVector()};

    // predict and update
    VectorFit::State last (lastVector);
    VectorFit::State current (vector);

    VectorFit::fit_vec<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
      pool,
      ntracks,
      transportForwardIterator.nextVector(),
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      last.m_updatedState.m_basePointer,
      last.m_updatedCovariance.m_basePointer,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );

    // Move requested data out and update data pointers
    if (out.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (out[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& node = track.m_nodes[pool[i].nodeIndex];

          VectorFit::State memslot (memseq.getNextElement());
          memslot.m_updatedState.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
          node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
          node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
        }
      }
    }
  });
}

// backward - pre
void backward_pre_iterations(
  const Scheduler& scheduler_post,
  VectorFit::MemManager& memseq, 
  NTracks& ntracks, 
  VectorFit::MemManagerReverseIterator& nodeParametersReverseIterator ) {
  // Backward fit and smoother
  //  Pre iterations
  std::for_each (scheduler_post.rbegin(), scheduler_post.rend(), [&] (decltype(scheduler_post[0])& s) {
    const auto& in = s.out;
    const auto& out = s.in;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Set memory location, do predict (always in a new vector, no need to swap)
    const auto& vector = memseq.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];

        // Update pointer
        node.m_backwardState.setBasePointer(vector + i);

        if (in[i]) {
          VectorFit::initialise<VectorFit::Op::Backward>(node, track.m_initialBackwardCovariance);
        } else {
          auto& prevnode = track.m_nodes[pool[i].nodeIndex+1];
          VectorFit::predict<VectorFit::Op::Backward, false>(node, prevnode);
        }
      }
    }

    const auto npVector = VectorFit::NodeParameters{nodeParametersReverseIterator.previousVector()};

    // Update
    VectorFit::State current (vector);
    VectorFit::update_vec<VECTOR_WIDTH> (
      pool,
      ntracks,
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );
  });
}

// backward-main
// some smooth iterations are done here
void backward_main_and_smoother_iterations (
  const Scheduler& scheduler_main,
  VectorFit::MemManager& memvecforward,
  VectorFit::MemManager& memvecbackward,
  VectorFit::MemManager& memvecsmoother,
  NTracks& ntracks,
  VectorFit::MemManagerReverseIterator& nodeParametersReverseIterator,
  VectorFit::MemManagerReverseIterator& transportBackwardIterator,
  VectorFit::MemManager& memseq ) {
  // Backward fit and smoother
  //  Main iterations
  // std::vector<VectorFit::SwapStore> swaps;
  memvecbackward.getNewVector();
  VectorFit::MemManagerReverseIterator forwardReverseIterator (memvecforward);
  std::for_each (scheduler_main.rbegin(), scheduler_main.rend(), [&] (decltype(scheduler_main[0])& s) {
    const auto& in  = s.out;
    const auto& out = s.in;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Feed the data we need in our vector
    const auto& lastVector = memvecbackward.getLastVector();
    if (in.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (in[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& prevnode = track.m_nodes[pool[i].nodeIndex+1];

          VectorFit::State memslot (lastVector + i);
          memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
        }
      }
    }

    // update the pointers requested
    const auto& vector = memvecbackward.getNewVector();
    const auto& smootherVector = memvecsmoother.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];

        node.m_backwardState.setBasePointer(vector + i);
        node.m_smoothState.setBasePointer(smootherVector + i);
      }
    }

    const auto npVector = VectorFit::NodeParameters{nodeParametersReverseIterator.previousVector()};

    // predict and update
    VectorFit::State last (lastVector);
    VectorFit::State current (vector);

    VectorFit::predict_vec<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
      pool,
      ntracks,
      transportBackwardIterator.previousVector(),
      last.m_updatedState.m_basePointer,
      last.m_updatedCovariance.m_basePointer,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer
      // TODO this needs the predicted states and covariances (add them here)
    );

    // Do the smoother *here*
    // This benefits from cache locality,
    // and removes the need of doing swapping to restore
    // the updated states
    
    // Fetch a new memvecsmoother vector
    VectorFit::SmoothState smooth (smootherVector);
    VectorFit::State forward (forwardReverseIterator.previousVector());

    // Smoother
    uint16_t smoother_result = VectorFit::smoother_vec<VECTOR_WIDTH> (
      forward.m_updatedState.m_basePointer,
      forward.m_updatedCovariance.m_basePointer,
      current.m_updatedState.m_basePointer,      // These are still the predicted state
      current.m_updatedCovariance.m_basePointer, // and covariance
      smooth.m_state.m_basePointer,
      smooth.m_covariance.m_basePointer
    );

    // Update residuals
    VectorFit::updateResiduals_vec (
      pool,
      ntracks,
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      smooth.m_state.m_basePointer,
      smooth.m_covariance.m_basePointer,
      smooth.m_residual,
      smooth.m_errResidual
    );

    // Update
    // TODO this needs the predicted state an covariance also
    VectorFit::update_vec<VECTOR_WIDTH> (
      pool,
      ntracks,
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );

  #ifdef DEBUG
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (not action[i]) {
        smoother_result |= 1 << i;
      }
    }
    if (smoother_result != VectorFit::ArrayGen::mask<VECTOR_WIDTH>()) {
      std::cout << "smoother errors: ";
      print(smoother_result);
      std::cout << std::endl;
    }
  #endif
    // Move requested data out and update data pointers
    if (out.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (out[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& node = track.m_nodes[pool[i].nodeIndex];

          VectorFit::State memslot (memseq.getNextElement());
          memslot.m_updatedState.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
          node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
          node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
          // swaps.push_back(
          //   VectorFit::SwapStore(
          //     memslot,
          //     node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>(),
          //     node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>()
          //   )
          // );
        }
      }
    }

    // // Restore the swapped updated state and covariance
    // for (auto& swap : swaps) {
    //   swap.store.m_updatedState.copy(swap.state);
    //   swap.store.m_updatedCovariance.copy(swap.covariance);
    // }
  });
}

// backward-post
void backward_post_iterations (
  const Scheduler& scheduler_pre,
  VectorFit::MemManager& memvecpost,
  NTracks& ntracks,
  VectorFit::MemManagerReverseIterator& nodeParametersReverseIterator,
  VectorFit::MemManagerReverseIterator& transportBackwardIterator,
  VectorFit::MemManager& memseq ) {
  // Backward fit
  //  Post iterations
  memvecpost.getNewVector();
  std::for_each (scheduler_pre.rbegin(), scheduler_pre.rend(), [&] (decltype(scheduler_pre[0])& s) {
    const auto& in  = s.out;
    const auto& out = s.in;
    const auto& action = s.action;
    const auto& pool = s.pool;

    // Feed the data we need in our vector
    const auto& lastVector = memvecpost.getLastVector();
    if (in.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (in[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& prevnode = track.m_nodes[pool[i].nodeIndex+1];

          VectorFit::State memslot (lastVector + i);
          memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
        }
      }
    }

    // update the pointers requested
    const auto& vector = memvecpost.getNewVector();
    for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
      if (action[i]) {
        auto& track = ntracks[pool[i].trackIndex];
        auto& node = track.m_nodes[pool[i].nodeIndex];

        node.m_backwardState.setBasePointer(vector + i);
      }
    }

    const auto npVector = VectorFit::NodeParameters{nodeParametersReverseIterator.previousVector()};

    // predict and update
    VectorFit::State last (lastVector);
    VectorFit::State current (vector);

    VectorFit::fit_vec<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
      pool,
      ntracks,
      transportBackwardIterator.previousVector(),
      npVector.m_referenceVector.m_basePointer,
      npVector.m_projectionMatrix.m_basePointer,
      npVector.m_referenceResidual,
      npVector.m_errorMeasure,
      last.m_updatedState.m_basePointer,
      last.m_updatedCovariance.m_basePointer,
      current.m_updatedState.m_basePointer,
      current.m_updatedCovariance.m_basePointer,
      current.m_chi2
    );

    // Move requested data out and update data pointers
    if (out.any()) {
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (out[i]) {
          auto& track = ntracks[pool[i].trackIndex];
          auto& node = track.m_nodes[pool[i].nodeIndex];

          VectorFit::State memslot (memseq.getNextElement());
          memslot.m_updatedState.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
          memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
          node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
          node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
        }
      }
    }
  });
}

// chi2 and latest smoothers
void latest_iterations (
  NTracks& ntracks,
  VectorFit::MemManager& memvecsmoother ) {
  // Calculate the chi2 a posteriori
  int bs = 0;
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& nodes = ntrack.m_nodes;
    ntrack.m_ndof = -ntrack.m_parent_nTrackParameters;
    for (int k=0; k<nodes.size(); ++k) {
      const VectorFit::FitNode& node = nodes[k];

      if (node.m_type == HitOnTrack) {
        ++ntrack.m_ndof;
        ntrack.m_forwardFitChi2  += node.get<VectorFit::Op::Forward, VectorFit::Op::Chi2>();
        ntrack.m_backwardFitChi2 += node.get<VectorFit::Op::Backward, VectorFit::Op::Chi2>();
      }
    }
  });

  // Smoother - not upstream iterations
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    for (int j=0; j<=ntrack.m_forwardUpstream; ++j) {
      ntrack.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
      VectorFit::smoother<false, true>(ntrack.m_nodes[j]);
      VectorFit::updateResiduals(ntrack.m_nodes[j]);
    }

    for (int j=0; j<=ntrack.m_backwardUpstream; ++j) {
      const unsigned element = ntrack.m_nodes.size() - j - 1;
      ntrack.m_nodes[element].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
      VectorFit::smoother<true, false>(ntrack.m_nodes[element]);
      VectorFit::updateResiduals(ntrack.m_nodes[element]);
    }
  });
}

// check results
void check_results (
    const Instance& instance,
    NTracks& ntracks,
    int bitEpsilonAllowed,
    unsigned verbose,
    unsigned eventno,
    unsigned instanceno,
    bool& allEqual,
    tbb::mutex& mutex
  ) {
  for (const auto& ntrack : ntracks) {

    const bool doForwardFit = instance.doForwardFit;
    const bool doBackwardFit = instance.doBackwardFit;
    const bool doBismooth = instance.doBismooth;
    const bool isOutlier = instance.isOutlier;

    const std::vector<FitNode>& result = instance.expectedResult[ntrack.m_index];

    std::vector<FitNode> track;
    for (const auto& node : ntrack.m_nodes) {
      track.push_back(node.operator FitNodeAOS());
    }
    track.back().m_totalChi2[Forward] = ChiSquare(ntrack.m_forwardFitChi2, ntrack.m_ndof);
    track.front().m_totalChi2[Backward] = ChiSquare(ntrack.m_backwardFitChi2, ntrack.m_ndof);
  #ifdef TBB_ON
    tbb::mutex::scoped_lock lock (mutex);
  #endif
    for (int l=0; l<track.size(); ++l) {
      int compareChi2 = -1;
      if (l==0) compareChi2 = Backward;
      else if (l==track.size()-1) compareChi2 = Forward;

      const bool comparison = compare(track[l], result[l], pow(2.0, ((double) bitEpsilonAllowed)), (verbose > 0), compareChi2);
      if (!comparison) {
        std::cout << "Mismatch in event #" << eventno << ", instance #" << instanceno
          << " (" << doForwardFit << ", " << doBackwardFit << ", " << doBismooth << ")"
          << ", track " << ntrack.m_index << ", node " << l << std::endl;
        allEqual = false;

        if (verbose > 1) {
          std::cout << std::endl;
          print(track[l]);
          std::cout << std::endl;
          print(result[l]);
          std::cout << std::endl;
        }
      }
    }
  }
}

// non-vectorized version of the Kalman 
void nonvec_kalman (
  NTracks& ntracks,
  VectorFit::MemManager& memseq,
  VectorFit::MemManager& memvecsmoother ) {
  // Forward fit
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {

    // this is just the first iteration, because we need to copy the covariance
    auto& nodes = ntrack.m_nodes;
    VectorFit::FitNode& node = nodes.front();
    node.m_forwardState.setBasePointer(memseq.getNextElement());
    VectorFit::fit<VectorFit::Op::Forward>(node, ntrack.m_initialForwardCovariance);

    // Before m_forwardUpstream
    // this is the pre
    for (int i=1; i<=ntrack.m_forwardUpstream; ++i) {
      VectorFit::FitNode& prevnode = nodes[i-1];
      VectorFit::FitNode& node = nodes[i];
      node.m_forwardState.setBasePointer(memseq.getNextElement());
      VectorFit::fit<VectorFit::Op::Forward, false>(node, prevnode);
    }

    // After m_forwardUpstream
    // this is the main && post
    for (int i=ntrack.m_forwardUpstream+1; i<nodes.size(); ++i) {
      VectorFit::FitNode& prevnode = nodes[i-1];
      VectorFit::FitNode& node = nodes[i];
      node.m_forwardState.setBasePointer(memseq.getNextElement());
      VectorFit::fit<VectorFit::Op::Forward, true>(node, prevnode);
    }
  });

  // Backward fit
  std::for_each(ntracks.begin(), ntracks.end(), [&] (VectorFit::Track& ntrack) {
    auto& nodes = ntrack.m_nodes;
    VectorFit::FitNode& node = nodes.back();
    node.m_backwardState.setBasePointer(memseq.getNextElement());
    VectorFit::fit<VectorFit::Op::Backward>(node, ntrack.m_initialBackwardCovariance);

    // Before m_backwardUpstream
    for (int i=1; i<=ntrack.m_backwardUpstream; ++i) {
      const int element = ntrack.m_nodes.size() - i - 1;
      VectorFit::FitNode& prevnode = nodes[element + 1];
      VectorFit::FitNode& node = nodes[element];
      node.m_backwardState.setBasePointer(memseq.getNextElement());
      VectorFit::fit<VectorFit::Op::Backward, false>(node, prevnode);
    }

    // After m_backwardUpstream
    for (int i=ntrack.m_backwardUpstream+1; i<nodes.size(); ++i) {
      const int element = ntrack.m_nodes.size() - i - 1;
      VectorFit::FitNode& prevnode = nodes[element + 1];
      VectorFit::FitNode& node = nodes[element];
      node.m_backwardState.setBasePointer(memseq.getNextElement());

      // we make the smoother in between to benefit from having the 
      // predict and update here
      VectorFit::predict<VectorFit::Op::Backward, true>(node, prevnode);

      // Do the smoother
      if (i < (ntrack.m_nodes.size() - ntrack.m_forwardUpstream - 1)) {
        node.m_smoothState.setBasePointer(memvecsmoother.getNextElement());
        VectorFit::smoother<true, true>(node);
        VectorFit::updateResiduals(node);
      }

      VectorFit::update<VectorFit::Op::Backward>(node);
    }
  });
}

namespace Store {
  thread_local VectorFit::MemManager memseq (21, 4096);
  thread_local VectorFit::MemManager memvecforward (21, 4096);
  thread_local VectorFit::MemManager memvecbackward (21, 4096);
  thread_local VectorFit::MemManager memvecpost (21, 4096);
  thread_local VectorFit::MemManager memvecsmoother (22, 4096);
}

int main(int argc, char **argv) {

  std::string foldername = "../events";
  // std::vector<std::vector<Instance>> events;
  int bitEpsilonAllowed = 50;
  bool printFitNodesWhenDifferent = false;
  unsigned numExperiments = 10000;
  unsigned maxNumFilesToOpen = 0;
  int minNumTracksInInstance = 0;
  bool checkResults = false;
  bool allEqual = true;
  unsigned verbose = 0;
  bool experimentMode = false;
  bool vectorised_opt = true;
  bool printTiming = false;
  unsigned warmupIterations = 0;
  unsigned minTrackBatchSize = 8;
  int event_chunk_size = 5;
  FileReader fr;
  char c;
  double startingTime = Timer::getCurrentTime();

  constexpr unsigned defaultReservedMemory = 1024;

  // parse command line arguments
  while (true) {
    c = getopt (argc, argv, "b:f:n:h?m:c:v:p:es:i:t:w:k:");
    if (c == -1 or c == 255 /* power8 */) break;

    switch (c) {
      case 'n':
        numExperiments = convertAmount(optarg);
        break;
      case 't':
        printTiming = (bool) atoi(optarg);
        break;
      case 'i':
        minTrackBatchSize = atoi(optarg);
        break;
      case 'f':
        foldername = optarg;
        break;
      case 'b':
        bitEpsilonAllowed = atoi(optarg);
        break;
      case 'm':
        maxNumFilesToOpen = atoi(optarg);
        break;
      case 'c':
        checkResults = (bool) atoi(optarg);
        break;
      case 'v':
        verbose = atoi(optarg);
        break;
      case 's':
        vectorised_opt = not ((bool) atoi(optarg));
        break;
      case 'w':
        warmupIterations = convertAmount(optarg);
        break;
      case 'k':
        event_chunk_size = atoi(optarg);
        break;
      case 'p':
      {
        const unsigned int percentile = atoi(optarg);
        // p = TimePrinter(true, percentile, 100 - percentile);
        break;
      }
      case 'e':
        experimentMode = true;
        break;
      case 'h':
      case '?':
        std::cout << "kalfit [-f foldername=events] [-w warmupIterations=0] [-n numExperiments=10000]"
          << " [-s sequential=0 (vectorised)] [-i minTrackBatchSize=8]"
          << " [-m maxNumFilesToOpen=0 (all)] [-e (print git checkout version=false)]"
          << " [-p topBottomPercentile=3] [-t printTiming=0]"
          << " [-c checkResults=0 (0,1) [-b bitEpsilonAllowed=50] [-v verbose=0 (0,1,2)]]" << std::endl;
        return 0;
      default:
        break;
    }
  }

  std::cout << "Running options: ";
  for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
  std::cout << std::endl << std::endl;

#if VECTOR_WIDTH == 1u
  vectorised_opt = false;
#endif

  if (experimentMode) {
    std::cout << "Reported version (git): " << shellexec("git rev-parse HEAD");
  }

  const int nb_thrs = 256;
  const int nb_nodes = 4;
  auto threads_topo = getThreadsTopology(nb_thrs);
  
  // print thread topology
  for (auto v : threads_topo) {
    for (auto e : v) {
      std::cout << e << ",";
    }
    std::cout << std::endl;
  }
  
  constexpr int thrs_per_node = nb_thrs/nb_nodes;
  const std::array<int, nb_nodes> numa_nodes = {0,1,2,3};
  const std::array< std::vector <int>, nb_nodes > all_threads = {
    threads_topo[0], 
    threads_topo[1], 
    threads_topo[2], 
    threads_topo[3]
  };

  struct Numa_node {
    int node_nb;
    std::vector<int> thread_nbs;
  };

  std::array<Numa_node, nb_nodes> xeon_phi_snc4;
  for (auto n : numa_nodes) {
    Numa_node aux;
    aux.node_nb = n;
    aux.thread_nbs = all_threads[n];
    xeon_phi_snc4[n] = aux; 
  }

  // init hwloc
  hwloc_topology_t topo;
  hwloc_topology_init(&topo);
  hwloc_topology_load(topo);

  // All NUMA nodes start computing at the same time
  std::atomic<int> barrier(0);
  std::atomic<int> barrier2(0);

  // init itt stuff
  // __itt_domain* domain = __itt_domain_create("Kalman.Domain.Global");
  // __itt_string_handle* handle_read_file = __itt_string_handle_create("read_file");

  // loop over the NUMA nodes
  std::vector< std::thread > numa_node_threads;
  for (int node=0; node<nb_nodes; ++node) {
    numa_node_threads.push_back(std::thread([&,node] () {

    // sequential_execution p{};
    parallel_execution_thr p{thrs_per_node};
    p.set_lockfree(true);

    // do the pinning manually
    hwloc_bitmap_t cpu_set = hwloc_bitmap_alloc();
    // pin first core of each NUMA node (xeon phi specific)
    hwloc_bitmap_set( cpu_set, node*16 ); 
    hwloc_set_cpubind( topo, cpu_set, HWLOC_CPUBIND_THREAD );

    // manually bind to NUMA nodes for events reading 
    hwloc_bitmap_t numa_set = hwloc_bitmap_alloc();
    // hwloc_bitmap_set( numa_set, node );
    hwloc_bitmap_set( numa_set, (node + 4) ); // only HBM
    hwloc_set_membind_nodeset(topo, numa_set, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_THREAD);

    // set affinity for GrPPI
    for (int i=0; i < thrs_per_node; ++i) {
      p.set_thread_affinity( i, { xeon_phi_snc4[node].thread_nbs[i] } );
    }

    for (int i=0; i < thrs_per_node; ++i) {
      std::vector<int> aux = {(xeon_phi_snc4[node].node_nb + 4) /*, xeon_phi_snc4[node].node_no*/};
      p.set_numa_affinity( i, aux );
    }

    // timers
    tbb::concurrent_vector<long long> copies_overhead;
    tbb::concurrent_vector<long long> processing_time;

    // Each NUMA node has its own events vector
    std::vector<std::vector<Instance>> events;

    // Read events
    VectorFit::MemManager memManager (41, defaultReservedMemory);
    {
      // __itt_task_begin(domain, __itt_null, __itt_null, handle_read_file);
      // files will be deleted - we don't want to waste a lot of memory on this
      std::vector<std::vector<uint8_t>> files;
      fr.readFilesFromFolder(files, foldername, maxNumFilesToOpen);
      for (const auto& f : files) {
        events.push_back(translateFileIntoEvent(f, memManager, checkResults, minTrackBatchSize));
      }
      // __itt_task_end(domain);
    }
    assert(events.size() > 0);

    // Reserve memory pointers
    VectorFit::MemManager nodeParametersStore {12, defaultReservedMemory};
    VectorFit::MemManager transportForwardStore {25, defaultReservedMemory};
    VectorFit::MemManager transportBackwardStore {25, defaultReservedMemory};

    // Iterators to know where we are reading
    std::vector<std::vector<VectorFit::MemManagerIterator>> nodeParametersIteratorInit (events.size());
    std::vector<std::vector<VectorFit::MemManagerIterator>> transportForwardIteratorInit (events.size());
    std::vector<std::vector<VectorFit::MemManagerReverseIterator>> nodeParametersReverseIteratorInit (events.size());
    std::vector<std::vector<VectorFit::MemManagerReverseIterator>> transportBackwardIteratorInit (events.size());
    std::vector<std::vector<std::array<std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>, 3>>> schedulers (events.size());

    initializeSchedulerAndIterators(
      events,
      schedulers,
      nodeParametersIteratorInit,
      transportForwardIteratorInit,
      nodeParametersReverseIteratorInit,
      transportBackwardIteratorInit,
      nodeParametersStore,
      transportForwardStore,
      transportBackwardStore
    );

    // Print some info for the events
    std::cout << "Read " << events.size() << " events" << std::endl;
    for (int i=0; i<events.size(); ++i) {
      const auto& event = events[i];

      std::cout << "Event #" << i << ":" << std::endl;
      printInfo(event);
    }
    
    // Prepare the memory manager
    unsigned totalNodes = 0;
    size_t maxNodeLength = 0;

    for (auto& event : events) {
      for (auto& instance : event) {
        totalNodes = std::max(totalNodes, [&] { unsigned acc=0; for (const auto& t : instance.ntracks) acc += t.m_nodes.size(); return acc; } ());
        maxNodeLength = std::max(maxNodeLength, [&] { size_t m=0; for (const auto& t : instance.ntracks) m = std::max(m, t.m_nodes.size()); return m; } ()) / 2.0;
      }
    }
    const unsigned reservedMem = totalNodes + maxNodeLength * VECTOR_WIDTH;
    const unsigned reservedMemSeq = vectorised_opt ? reservedMem : reservedMem * 2;
    const unsigned reservedMemVec = vectorised_opt ? reservedMem : 1;

    const int numberOfThreads = thrs_per_node;
    tbb::mutex mutex;

    int event_counter = 0;
    const int event_chunk = event_chunk_size;

    // __itt_string_handle* handle_input = __itt_string_handle_create("input_filter");
    // __itt_string_handle* handle_process_chunk = __itt_string_handle_create("process_chunk");

    // producer for warmup
    const auto inputWarmupFn = [&] () {
      if (event_counter >= warmupIterations) {
        return optional<MinimalData>{};
      }
      auto eventno = event_counter;
      event_counter+=event_chunk;

      return optional<MinimalData>{MinimalData(eventno, 0)};
    };

    // consumer
    const auto processWarmupFn = [&] (MinimalData pipeData) {

      for(auto ev = 0; ev < event_chunk; ev++) {
        //Determine the event id
        auto eventno = pipeData.eventno +ev;
        if(eventno >= numExperiments) break;
        eventno = eventno % events.size();

        for(auto instanceno=0; instanceno<events[eventno].size(); ++instanceno) {

          VectorFit::MemManagerIterator nodeParametersIterator (nodeParametersIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerIterator transportForwardIterator (transportForwardIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerReverseIterator nodeParametersReverseIterator (nodeParametersReverseIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerReverseIterator transportBackwardIterator (transportBackwardIteratorInit[eventno][instanceno]);

          auto ntracks = events[eventno][instanceno].ntracks; // copy just the ntracks

          // Default to sequential execution if number of tracks is less than vector width
          const bool vectorised = (ntracks.size() >= VECTOR_WIDTH) ? vectorised_opt : false;

          nonvec_kalman(
            ntracks,
            Store::memseq,
            Store::memvecsmoother
          );

          Store::memseq.reset();
          Store::memvecforward.reset();
          Store::memvecbackward.reset();
          Store::memvecsmoother.reset();
          Store::memvecpost.reset();

        } // instances for()
      } // event_chunk for()
    }; // lambda processWarmupFn()

    // general producer
    const auto inputFn = [&] () {

      // __itt_task_begin(domain, __itt_null, __itt_null, handle_input);

      if (event_counter >= numExperiments) {
        return optional<MinimalData>{};
      }
      auto eventno = event_counter;
      event_counter+=event_chunk;

      // __itt_task_end(domain);

      return optional<MinimalData>{MinimalData(eventno, 0)};
    };

    // consumer
    const auto processFn = [&] (MinimalData pipeData) {

      // __itt_task_begin(domain, __itt_null, __itt_null, handle_process_chunk);

      for(auto ev = 0; ev < event_chunk; ev++) {
        //Determine the event id
        auto eventno = pipeData.eventno +ev;
        if(eventno >= numExperiments) break;
        eventno = eventno % events.size();

        for(auto instanceno=0; instanceno<events[eventno].size(); ++instanceno) {

          auto tcopy1 = std::chrono::high_resolution_clock::now();

          VectorFit::MemManagerIterator nodeParametersIterator (nodeParametersIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerIterator transportForwardIterator (transportForwardIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerReverseIterator nodeParametersReverseIterator (nodeParametersReverseIteratorInit[eventno][instanceno]);
          VectorFit::MemManagerReverseIterator transportBackwardIterator (transportBackwardIteratorInit[eventno][instanceno]);

          auto ntracks = events[eventno][instanceno].ntracks; // copy just the ntracks

          auto tcopy2 = std::chrono::high_resolution_clock::now();
          copies_overhead.push_back(std::chrono::duration_cast<std::chrono::milliseconds>( tcopy2 - tcopy1 ).count());

          auto tproc1 = std::chrono::high_resolution_clock::now();

          // Default to sequential execution if number of tracks is less than vector width
          const bool vectorised = (ntracks.size() >= VECTOR_WIDTH) ? vectorised_opt : false;

          if (vectorised) {
            forward_pre_iterations(
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Pre],
              Store::memvecforward,
              ntracks,
              nodeParametersIterator
            );

            forward_main_iterations(
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Main],
              Store::memvecforward,
              ntracks,
              nodeParametersIterator,
              transportForwardIterator,
              Store::memseq
            );

            forward_post_iterations(
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Post],
              Store::memvecpost,
              ntracks,
              nodeParametersIterator,
              transportForwardIterator,
              Store::memseq
            );

            backward_pre_iterations(
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Post],
              Store::memseq,
              ntracks,
              nodeParametersReverseIterator
            );

            backward_main_and_smoother_iterations (
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Main],
              Store::memvecforward,
              Store::memvecbackward,
              Store::memvecsmoother,
              ntracks,
              nodeParametersReverseIterator,
              transportBackwardIterator,
              Store::memseq
            );

            backward_post_iterations (
              schedulers[eventno][instanceno][VectorFit::Sch::Num::Pre],
              Store::memvecpost,
              ntracks,
              nodeParametersReverseIterator,
              transportBackwardIterator,
              Store::memseq
            );

            latest_iterations(
              ntracks,
              Store::memvecsmoother
            );

            if (checkResults) {
              check_results(
                events[eventno][instanceno],
                ntracks,
                bitEpsilonAllowed,
                verbose,
                eventno,
                instanceno,
                allEqual,
                mutex
              );
            }
          
            Store::memseq.reset();
            Store::memvecforward.reset();
            Store::memvecbackward.reset();
            Store::memvecsmoother.reset();
            Store::memvecpost.reset();
          }
          else {
            nonvec_kalman(
              ntracks,
              Store::memseq,
              Store::memvecsmoother
            );
          }

          auto tproc2 = std::chrono::high_resolution_clock::now();
          processing_time.push_back(std::chrono::duration_cast<std::chrono::milliseconds>( tproc2 - tproc1 ).count());

        } // instances for()
      } // event_chunk for()

      // __itt_task_end(domain);

    }; // lambda processFn()

    // start computing all events at the same time
    barrier++;
    while (barrier != nb_nodes);

    std::cout << "Running experiments on NUMA node: " << xeon_phi_snc4[node].node_nb << std::endl;

    // warm-up
    std::cout << "NUMA node " << xeon_phi_snc4[node].node_nb << ": warming up" << std::endl;
    if (warmupIterations > 0) {
      farm(p,inputWarmupFn,processWarmupFn);
    }

    std::cout << "NUMA node " << xeon_phi_snc4[node].node_nb << ": processing!" << std::endl;
    // __itt_resume();

    // start computing all events at the same time
    barrier2++;
    while (barrier2 != nb_nodes);

    std::chrono::high_resolution_clock::time_point tScale1 = std::chrono::high_resolution_clock::now();

    event_counter = 0;

    farm(p,inputFn,processFn);

	  // __itt_pause();
    std::chrono::high_resolution_clock::time_point tScale2 = std::chrono::high_resolution_clock::now();

    std::cout << std::endl;

    if (checkResults) {
      if (allEqual) {
        std::cout << "Check results succeeded!" << std::endl
          << "All of the experiments gave the expected results" << std::endl;
      }
      else {
        std::cout << "Check results failed" << std::endl
          << "Some experiments did not yield the expected results" << std::endl
          << "Try running with -c 1 -v 1 to see what happens" << std::endl;
      }
    }

    int processed_instances = processing_time.size();
    auto total_copies_overhead = std::accumulate(copies_overhead.begin(), copies_overhead.end(), 0);
    auto total_processing_time = std::accumulate(processing_time.begin(), processing_time.end(), 0);

    // Timer to measure how it scales (time to execute everything)
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( tScale2 - tScale1 ).count();

    std::cout << "NUMA node: " << xeon_phi_snc4[node].node_nb << std::endl;
    std::cout << "n_inst,f_amount,total_time,copies_time,processing_time" << std::endl;
    std::cout << processed_instances << "," 
              << events.size() << "," 
              << duration << "," 
              << total_copies_overhead << ","
              << total_processing_time << std::endl;

    // std::cout << "Total NUMA node: " << xeon_phi_snc4[node].node_no << " - Time to execute(usecs): " << duration << std::endl;

  })); // std::thread
  } // over vector<std::thread> for()

  // join the threads
  std::for_each(numa_node_threads.begin(), numa_node_threads.end(), [](std::thread &t) {
    t.join();
  });

  return 0;
}
